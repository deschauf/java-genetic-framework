package pack;
public class Fitness implements FitnessFunction
{

	/*
	 * Problematic : Find the int which major f(x)= 1/4*(15*x�-x^3)+4 on [0; 16]
	 */	
	public Fitness()
	{
	}
	
	/*
	 * getFitness
	 * Parameters : Chromosome
	 * Return value : integer
	 * Goal : return a fitness of the current chromosome
	 */
	
	public int getFitness(Chromosome Chr)
	{
		int value = binaryToDecimale(Chr);
		return (value*value*15-value*value*value-4); 
	}	
	
	/*
	 * binaryToDecimale
	 * Parameters : Chromosome
	 * Return value : integer
	 * Goal : Convert a table of boolean to a Decimal 
	 */
	
	public int binaryToDecimale(Chromosome Chr)
	{
		boolean a = false;
		int sum = 0, binaryValue = 0;
		for (int i = 0; i<Chr.getChromosomeList().size(); i++)
		{
			a = (boolean) Chr.getChromosomeList().get(i).getIGene().getAllel(1);
			if (a == true)
			{
				binaryValue = (int)Math.pow(2,(Chr.getChromosomeList().size()-i-1));	
			}					
			sum += binaryValue;
			binaryValue = 0;
		}		
		return sum;
	}
}
