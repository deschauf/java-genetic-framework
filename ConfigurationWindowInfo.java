/*
 *     Copyright (C) <2013>  <DESCHAU Flavien VASSAL Adrien>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package pack;

public class ConfigurationWindowInfo {
	
	/**
	 * boolean which represent a choice of the user
	 */
	private boolean boolCreation, boolMutation, boolCrossover, boolGene;
	
	/**
	 * Integer which represent a number given by the user
	 */
	private int numberOfChromosme, chromosomeLength, numberOfGeneration, numberSelectionOperator;
	
	/**
	 * Default constructor
	 */
	ConfigurationWindowInfo()
	{		
	}
	
	/**
	 * Overloading of the constructor
	 * 
	 * @param creation
	 * 		false if it's the default operator and true is it's a custom operator
	 * @param mutation
	 * 		false if it's the default operator and true is it's a custom operator
	 * @param crossover
	 * 		false if it's the default operator and true is it's a custom operator
	 * @param gene
	 * 		false if it's the default operator and true is it's a custom operator
	 * @param valueNumberOfChromosome
	 * @param valueChromosomeLength
	 * @param valueNumberOfGeneration
	 * @param valueNumberSelection
	 * 		Integer which represent which operator of selection must be used
	 */
	ConfigurationWindowInfo(boolean creation, boolean mutation, boolean crossover,boolean gene, int valueNumberOfChromosome, int valueChromosomeLength,int valueNumberOfGeneration, int valueNumberSelection )
	{
		boolCreation = creation;
		boolMutation = mutation;
		boolCrossover = crossover;
		boolGene = gene;
		numberOfChromosme = valueNumberOfChromosome;
		numberOfGeneration = valueNumberOfGeneration;
		chromosomeLength = valueChromosomeLength;		
		numberSelectionOperator = valueNumberSelection;
	}
	
	public int getnumberOfChromosome()
	{
		return numberOfChromosme;
	}
	
	public int getnumberOfGeneration()
	{
		return numberOfGeneration;
	}
	
	public int getChromosomeLength()
	{
		return chromosomeLength;
	}

	public boolean getBoolCreation()
	{
		return boolCreation;
	}
	
	public boolean getBoolMutation()
	{
		return boolMutation;
	}
	
	public boolean getBoolCrossover()
	{
		return boolCrossover;
	}
	
	public boolean getBoolGene()
	{
		return boolGene;
	}
	
	public int getNumberSelectionOperator()
	{
		return numberSelectionOperator;
	}
}
