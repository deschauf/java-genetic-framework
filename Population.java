/*
 *     Copyright (C) <2013>  <DESCHAU Flavien VASSAL Adrien>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package pack;


import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

public class Population extends Chromosome

{
	/**
	 * Number of chromosome
	 */
	private int numberOfChromosome;
	
	/**
	 * Length of the chromosome
	 */
	private int chromosomeLength;
	
	/**
	 * Total number of generation
	 */
	private int numberOfGeneration=0;
	
	/**
	 * Population of chromosome
	 */
	private Chromosome myPopulation [];
	
	/**
	 * A Configuration object
	 * 
	 * @see Configuration
	 */
	private Configuration config;
	
	public Population()
	{		
	}

	/**
	 * Initialize the population using the Configuration object
	 * 
	 * @param conf
	 * 
	 * @see Configuration
	 */
	public Population(Configuration conf)
	{
		super(conf);
		config = conf;
		numberOfChromosome = conf.getNumberOfChromosome();
		chromosomeLength = conf.getChromosomeLength();
		numberOfGeneration = conf.getNumberOfGeneration();
		myPopulation= new Chromosome[numberOfChromosome];
		for (int i=0;i<myPopulation.length;i++)
		{	
			myPopulation[i] = new Chromosome(conf);
		}
	}
	
	/**
	 * Calculate the average of the fitness of the current population
	 * 
	 * @return an integer which represent the average of the fitness 
	 */
	int fitnessAverage()
	{
		int Average =0;
		for(int i=0;i<myPopulation.length;i++)
		{
			Average += myPopulation[i].getChromosomeFitness();
		}
		return(int)(Average/myPopulation.length);
	}
	
	/**
	 * Generate a cycle of evolution with the current population 
	 * 
	 * @see CreateOperator
	 * @see MutateOperator
	 * @see CrossoverOperator
	 */	
	void evolve()	
	{		
		FitnessFunction fit = (FitnessFunction) config.getFitnessOperator();
		ISelectionOperator select = (ISelectionOperator) config.getSelectionOperator();
		IMutateOperator mute = (IMutateOperator) config.getMutationOperator();
		ICrossoverOperator cross = (ICrossoverOperator) config.getCrossoverOperator();

		Chromosome mu[]=mute.mutation(config, myPopulation);
		Chromosome cr[]=cross.crossover(config, myPopulation);
		Chromosome popAdd[] = new Chromosome[mu.length+cr.length+myPopulation.length];
		popAdd = this.addPopulation(mu, cr);		
		for(int i=0;i<popAdd.length;i++)
		{
			popAdd[i].setChromosomeFitness(fit.getFitness(popAdd[i]));
		}
		myPopulation = select.selection(popAdd, config);
	}
	
	
	/**
	 * return the Chromosome which have the best fitness in the current population
	 * 
	 * @return a chromosome which is the actual best solution
	 * 
	 * @see Chromosome
	 */
	Chromosome bestSolution()
	{
		int index = 0;
		int fitness = 0;
		for (int i=0;i<myPopulation.length; i++)
		{
			if(myPopulation[i].getChromosomeFitness()>fitness)
			{
				fitness = myPopulation[i].getChromosomeFitness();
				index = i;
			}
		}
		return myPopulation[index];
	}
	
	/**
	 * Create the first population with a creator given by the configuration Object
	 */
	void createRndPopulation()
	{
		FitnessFunction fit = (FitnessFunction) config.getFitnessOperator();
		ICreateOperator create = (ICreateOperator) (config.getCreateOperator());
		myPopulation = create.creatPopulation(config);
		for(int i=0;i<myPopulation.length;i++)
		{
			myPopulation[i].setChromosomeFitness(fit.getFitness(myPopulation[i]));
		}
	}
	
	/**
	 * Convert a string in to a table of boolean
	 * <p>
	 * This method is only use for boolean gene
	 * <p>
	 * @param value
	 * 		The string to convert
	 * @return A table of boolean which represent a chromosome
	 */	
	boolean[] stringToBoolean(String value)
	{
		boolean[] myChromosome = new boolean[chromosomeLength];
		int save=0;
		if((chromosomeLength-value.length())>0)
		{
			for(int i=0;i<(chromosomeLength-value.length());i++)
			{
				myChromosome[i]=false;
				save++;
			}
			for(int i=0;i<value.length();i++)
			{
				if(value.charAt(i)=='0')
				{
					myChromosome[i+save]=false;
				}
				if(value.charAt(i)=='1')
				{
					myChromosome[i+save]=true;
				}
			}
		}
		else
			for(int i=0;i<chromosomeLength;i++)
			{
				if(value.charAt(i)=='0')
				{
					myChromosome[i]=false;
				}
				if(value.charAt(i)=='1')
				{
					myChromosome[i]=true;
				}
			}			
		return myChromosome;
	}
	
	/**
	 * Put the current, the mutate and the crossover population in the same table
	 * 
	 * @param mutate
	 * 		The mutate population
	 * @param crossovered
	 * 		The crossover population
	 * @return a table of all the population (actual+mutate+crossover)
	 * 
	 * @see MutateOperator
	 * @see CrossoverOperator
	 */
	Chromosome[] addPopulation(Chromosome mutate[],Chromosome crossovered[])
	{
		 Chromosome mixPop[] = new Chromosome[mutate.length+crossovered.length+this.myPopulation.length];
		 for (int i=0;i<mixPop.length;i++)
			{	
				mixPop[i] = new Chromosome(config);
			}
		 
		 int j = 0;
		 int k = 0;
		 if(mutate.length!=0)
		 {
			 while(j<mutate.length)
			 {
				 mixPop[j] = mutate[j];
				 j++;
			 }
			 k = j;
			 j = 0;
		 }	 	 
		 if(crossovered.length != 0)
		 {
			 while(j<crossovered.length)
			 {
				mixPop[k+j] = crossovered[j];	
				j++;
			 }
			 k += j;
			 j = 0;
		 }
		 while(j<this.numberOfChromosome)
		 {		
			 for(int m=0;m<chromosomeLength;m++)
			 {
			 mixPop[k+j].setGeneAt(m,myPopulation[j].getGeneAt(m));   
			 }
			 j++;
		 }		 	
		return mixPop;
	 }
	
	/**
	 * Right the actual population in a file
	 * 
	 * @param myPop
	 * 		The actual population
	 * @param result
	 * 		The file to print in
	 * @param actualGeneration
	 * 		The actual number of generation
	 * @throws IOException
	 */
	
	void printInFile(Population myPop, File result, int actualGeneration) throws IOException
	{	
		BufferedWriter writer = new BufferedWriter(new FileWriter(result, true));
		if (actualGeneration==0&&(result.length()==0))
		{
			writer.write("Generation n�"+ " \t \tChromosome n�" +"\t \t"+"Chromosome Value\r\n");
		}
		switch(config.getGeneType())
		{
		case 1 :
			char [] toString = new char[chromosomeLength];
			for (int i=0;i< myPop.myPopulation.length;i++)
			{
				for(int j=0; j<chromosomeLength;j++)
				{
					if((Boolean)myPop.myPopulation[i].getGeneAt(j)==true)
					{
						toString[j]='1';
					}
					else
					{
						toString[j]='0';
					}
				}
				String toPrint = new String (toString);
				writer.write(actualGeneration+"\t\t\t" + i+"\t\t\t"+toPrint+"\r\n");	
			}
		break;
		case 2 :
				for(int i=0;i<myPop.myPopulation.length;i++)
				{
					ArrayList<Character> toStringTemp = new ArrayList<Character>();	

					for(int j=0; j<chromosomeLength;j++)
					{
						char [] save = new char[String.valueOf(myPop.myPopulation[i].getGeneAt(j)).length()];
						save = String.valueOf((Integer)myPop.myPopulation[i].getGeneAt(j)).toCharArray();
						for(int k=0;k<save.length;k++)
						{
							toStringTemp.add(save[k]);
						}
					}
					char [] toString1 = new char[toStringTemp.size()];
					for(int j=0;j<toStringTemp.size();j++)
					{
						toString1[j]=toStringTemp.get(j);
					}
					String toPrint = new String (toString1);
					writer.write(actualGeneration+"\t\t\t" + i+"\t\t\t"+toPrint+"\r\n");	
				}

		break;
		case 3 :
			for(int i=0;i<myPop.myPopulation.length;i++)
			{
				ArrayList<Character> toStringTemp = new ArrayList<Character>();	

				for(int j=0; j<chromosomeLength;j++)
				{
					char [] save = new char[String.valueOf(myPop.myPopulation[i].getGeneAt(j)).length()];
					save = String.valueOf((Double)myPop.myPopulation[i].getGeneAt(j)).toCharArray();
					for(int k=0;k<save.length;k++)
					{
						toStringTemp.add(save[k]);
					}
				}
				char [] toString2 = new char[toStringTemp.size()];
				for(int j=0;j<toStringTemp.size();j++)
				{
					toString2[j]=toStringTemp.get(j);
				}
				String toPrint = new String (toString2);
				writer.write(actualGeneration+"\t\t\t" + i+"\t\t\t"+toPrint+"\r\n");	
			}
		break;
		case 4 :
			for(int i=0;i<myPop.myPopulation.length;i++)
			{
				ArrayList<Character> toStringTemp = new ArrayList<Character>();	

				for(int j=0; j<chromosomeLength;j++)
				{
					char [] save = new char[String.valueOf(myPop.myPopulation[i].getGeneAt(j)).length()];
					save = String.valueOf((Long)myPop.myPopulation[i].getGeneAt(j)).toCharArray();
					for(int k=0;k<save.length;k++)
					{
						toStringTemp.add(save[k]);
					}
				}
				char [] toString3 = new char[toStringTemp.size()];
				for(int j=0;j<toStringTemp.size();j++)
				{
					toString3[j]=toStringTemp.get(j);
				}
				String toPrint = new String (toString3);
				writer.write(actualGeneration+"\t\t\t" + i+"\t\t\t"+toPrint+"\r\n");	
			}
		break;
		case 5 :
			for(int i=0;i<myPop.myPopulation.length;i++)
			{
				ArrayList<Character> toStringTemp = new ArrayList<Character>();	

				for(int j=0; j<chromosomeLength;j++)
				{
					char [] save = new char[String.valueOf(myPop.myPopulation[i].getGeneAt(j)).length()];
					save = String.valueOf((Float)myPop.myPopulation[i].getGeneAt(j)).toCharArray();
					for(int k=0;k<save.length;k++)
					{
						toStringTemp.add(save[k]);
					}
				}
				char [] toString4 = new char[toStringTemp.size()];
				for(int j=0;j<toStringTemp.size();j++)
				{
					toString4[j]=toStringTemp.get(j);
				}
				String toPrint = new String (toString4);
				writer.write(actualGeneration+"\t\t\t" + i+"\t\t\t"+toPrint+"\r\n");	
			}
		break;
		case 6 :
			for(int i=0;i<myPop.myPopulation.length;i++)
			{
				ArrayList<Character> toStringTemp = new ArrayList<Character>();	

				for(int j=0; j<chromosomeLength;j++)
				{
					char [] save = new char[String.valueOf(myPop.myPopulation[i].getGeneAt(j)).length()];
					save = String.valueOf((Short)myPop.myPopulation[i].getGeneAt(j)).toCharArray();
					for(int k=0;k<save.length;k++)
					{
						toStringTemp.add(save[k]);
					}
				}
				char [] toString5 = new char[toStringTemp.size()];
				for(int j=0;j<toStringTemp.size();j++)
				{
					toString5[j]=toStringTemp.get(j);
				}
				String toPrint = new String (toString5);
				writer.write(actualGeneration+"\t\t\t" + i+"\t\t\t"+toPrint+"\r\n");	
			}
		break;
		case 7 :
			for(int i=0;i<myPop.myPopulation.length;i++)
			{
				ArrayList<Character> toStringTemp = new ArrayList<Character>();	

				for(int j=0; j<chromosomeLength;j++)
				{
					char [] save = new char[String.valueOf(myPop.myPopulation[i].getGeneAt(j)).length()];
					save = String.valueOf((Byte)myPop.myPopulation[i].getGeneAt(j)).toCharArray();
					for(int k=0;k<save.length;k++)
					{
						toStringTemp.add(save[k]);
					}
				}
				char [] toString6 = new char[toStringTemp.size()];
				for(int j=0;j<toStringTemp.size();j++)
				{
					toString6[j]=toStringTemp.get(j);
				}
				String toPrint = new String (toString6);
				writer.write(actualGeneration+"\t\t\t" + i+"\t\t\t"+toPrint+"\r\n");	
			}
		break;
		}
		
		 writer.close();
	}
	
	/**
	 * 	Initialize a population by the last population in your result file
	 * @param result
	 * 		File to use for the initialization
	 * @param nbChr
	 * 		Actual number of chromosome
	 * @throws FileNotFoundException
	 */
	@SuppressWarnings("resource")
	void initialisePopFromFile(File result, int nbChr) throws FileNotFoundException
	{
		int nbLine=0;
		Scanner sc = new Scanner(result);
		sc.nextLine();
		while (sc.hasNext())
		{
			if (sc.hasNextInt())
			{
				sc.nextInt();
				sc.nextInt();
				sc.next();
			    nbLine++;
			}
		}
		sc = new Scanner(result);
		int place=0;
		String str =sc.nextLine();
		for(int k=0;k<nbLine;k++)
		{
			
			if (k>=(nbLine-nbChr))
			{;
				sc.nextInt();
				sc.nextInt();
			    str= sc.next();
			    myPopulation[place]=charToChr(str.toCharArray());
			    place++;
			}
			else
			{
;
				str =sc.nextLine();
			}
		}
		sc.close();
	}
	
	/**
	 * set your chromosome with a table of char '0' and '1'
	 * <p>
	 * This function is only used for the boolean gene
	 * <p>
	 * @param chr
	 * 		Table of boolean to use for the setting
	 * @return a chromosome
	 * 
	 * @see Chromosome
	 */
	Chromosome charToChr(char chr[])
	{
		Chromosome chromosome= new Chromosome(config);
		for(int i=0; i<chr.length;i++)
		{
			if (chr[i]=='0')
			{
				chromosome.setGeneAt(i, false);
			}
			else if (chr[i]=='1')
			{
				chromosome.setGeneAt(i, true);
			}
		}
		return chromosome;
	}
	
	
	
	/*
	 * get Functions
	 */
	public Chromosome[] getAllChromosome()
	{
		return myPopulation;
	}
	
	public Chromosome getChromosome(int index)
	{
		return myPopulation[index] ;
	}
	
	public int getChromosomeLength()
	{
		return chromosomeLength;
	}
	
	public int getNumberOfGeneration()
	{
		return numberOfGeneration;
	}
	
	/*
	 * set Function
	 */
	
	public void setChromosomePopulation(Chromosome actualChromosome, int index)
	{
		myPopulation[index]=actualChromosome;
	}
}