/*
 *     Copyright (C) <2013>  <DESCHAU Flavien VASSAL Adrien>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package pack;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

public class ConfigurationWindow extends JDialog
{

	private static final long serialVersionUID = 1L;
	/**
	 * Object which is used to recover the information of the configuration window
	 * 
	 * @see ConfigurationWindowInfo
	 */
	private ConfigurationWindowInfo confWindInf = new ConfigurationWindowInfo();
	
	/**
	 * Graphical object
	 */
	private JRadioButton choiceGene1, choiceGene2, choiceCreation1, choiceCreation2, choiceMutation1, choiceMutation2, choiceCrossover1, choiceCrossover2, choiceSelection1, choiceSelection2, choiceSelection3, choiceSelection4 ;
	
	/**
	 * Graphical object
	 */
	private JTextField number, length, generation;
	
	/**
	 * Graphical object
	 */
	private JLabel numberLabel, lengthLabel, generationLabel;
	
	/**
	 * Validation boolean
	 * <p>
	 * The value is true when the configuration is correct
	 * <p>
	 */
	private boolean confOK;
	
	/**
	 * Overloading constructor 
	 * @param parent
	 * @param title
	 * @param modal
	 */
	public ConfigurationWindow(JFrame parent, String title, boolean modal)
	{
	    super(parent, title, modal);
	    confOK=false;
	    this.setSize(600, 400);
	    this.setLocationRelativeTo(null);
	    this.setResizable(false);
	    this.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
	    this.initComponent();
	}

	/**
	 * Default Constructor
	 * 
	 */
	public ConfigurationWindow()
	{	
		confOK=false;
	}
	
	/**
	 * Get the object which contain all the information about the configuration
	 * 
	 * @return Object containing information about the configuration 
	 * 
	 * @see ConfigurationWindowInfo
	 */
	public ConfigurationWindowInfo showZDialog()
	{
	    return this.confWindInf;      
    }
	
	/**
	 * Creation of the configuration window
	 * <p>
	 * This method also manage the detection of the user's actions,
	 * and with the recovery of the information.
	 * <p>
	 */
	private void initComponent()
	{
		 /*Number of chromosome*/		 
		 JPanel panNumber = new JPanel();
		 panNumber.setBackground(Color.white);
		 panNumber.setPreferredSize(new Dimension(220, 60));
		 number = new JTextField();
		 number.setPreferredSize(new Dimension(100, 25));
		 panNumber.setBorder(BorderFactory.createTitledBorder("Number of chromosome"));
		 numberLabel = new JLabel("Value :");
		 panNumber.add(numberLabel);
		 panNumber.add(number);
		    
		 /*chromosome Length*/		 
		 JPanel panLength = new JPanel();
		 panLength.setBackground(Color.white);
		 panLength.setPreferredSize(new Dimension(220, 60));
		 length = new JTextField();
		 length.setPreferredSize(new Dimension(100, 25));
		 panLength.setBorder(BorderFactory.createTitledBorder("Chromosome Length"));
		 lengthLabel = new JLabel("Value :");
		 panLength.add(lengthLabel);
		 panLength.add(length);
		  
		 /*Number of Generation*/		 
		 JPanel panGeneration = new JPanel();
		 panGeneration.setBackground(Color.white);
		 panGeneration.setPreferredSize(new Dimension(420, 60));
		 generation = new JTextField();
		 generation.setPreferredSize(new Dimension(100, 25));
		 panGeneration.setBorder(BorderFactory.createTitledBorder("Number of generation"));
		 generationLabel = new JLabel("Value :");
		 panGeneration.add(generationLabel);
		 panGeneration.add(generation);
		 
		 /*Gene choice*/
		 JPanel panGene = new JPanel();
		 panGene.setBackground(Color.white);
		 panGene.setBorder(BorderFactory.createTitledBorder("Gene choice"));
		 panGene.setPreferredSize(new Dimension(220, 60));	
		 choiceGene1 = new JRadioButton("Default Gene");
		 choiceGene1.setSelected(true);
		 choiceGene2 = new JRadioButton("Custum Gene");
		 ButtonGroup bg = new ButtonGroup();
		 bg.add(choiceGene1);
		 bg.add(choiceGene2);
		 panGene.add(choiceGene1);
		 panGene.add(choiceGene2);	
		 
		 /*Creation choice*/
		 JPanel panCreation = new JPanel();
		 panCreation.setBackground(Color.white);
		 panCreation.setBorder(BorderFactory.createTitledBorder("Creator operator choice"));
		 panCreation.setPreferredSize(new Dimension(280, 60));	
		 choiceCreation1 = new JRadioButton("Default Creator");
		 choiceCreation1.setSelected(true);
		 choiceCreation2 = new JRadioButton("Custum Creator");
		 ButtonGroup bg1 = new ButtonGroup();
		 bg1.add(choiceCreation1);
		 bg1.add(choiceCreation2);
		 panCreation.add(choiceCreation1);
		 panCreation.add(choiceCreation2);	
		 
		 /*Mutation choice*/
		 JPanel panMutation = new JPanel();
		 panMutation.setBackground(Color.white);
		 panMutation.setBorder(BorderFactory.createTitledBorder("Mutation operator choice"));
		 panMutation.setPreferredSize(new Dimension(280, 60));
		 choiceMutation1 = new JRadioButton("Default Operator");
		 choiceMutation1.setSelected(true);
		 choiceMutation2 = new JRadioButton("Custum Operator");
		 ButtonGroup bg2 = new ButtonGroup();
		 bg2.add(choiceMutation1);
		 bg2.add(choiceMutation2);
		 panMutation.add(choiceMutation1);
		 panMutation.add(choiceMutation2);	
		 
		 /*Crossover choice*/
		 JPanel panCrossover = new JPanel();
		 panCrossover.setBackground(Color.white);
		 panCrossover.setBorder(BorderFactory.createTitledBorder("Crossover operator choice"));
		 panCrossover.setPreferredSize(new Dimension(280, 60));
		 choiceCrossover1 = new JRadioButton("Default Operator");
		 choiceCrossover1.setSelected(true);
		 choiceCrossover2 = new JRadioButton("Custum Operator");
		 ButtonGroup bg3 = new ButtonGroup();
		 bg3.add(choiceCrossover1);
		 bg3.add(choiceCrossover2);
		 panCrossover.add(choiceCrossover1);
		 panCrossover.add(choiceCrossover2);
		 
		 /*Selection choice*/
		 JPanel panSelection = new JPanel();
		 panSelection.setBackground(Color.white);
		 panSelection.setBorder(BorderFactory.createTitledBorder("Selection operator choice"));
		 panSelection.setPreferredSize(new Dimension(550, 60));
		 choiceSelection1 = new JRadioButton("Tournament proba");
		 choiceSelection1.setSelected(true);
		 choiceSelection2 = new JRadioButton("Tourneament");
		 choiceSelection3 = new JRadioButton("Biased whell");
		 choiceSelection4 = new JRadioButton("Custum Operator");
		 ButtonGroup bg4 = new ButtonGroup();
		 bg4.add(choiceSelection1);
		 bg4.add(choiceSelection2);
		 bg4.add(choiceSelection3);
		 bg4.add(choiceSelection4);
		 panSelection.add(choiceSelection1);
		 panSelection.add(choiceSelection2);
		 panSelection.add(choiceSelection3);
		 panSelection.add(choiceSelection4);
		 
		 JPanel content = new JPanel();
		 content.setBackground(Color.white);	
		 content.add(panLength);
		 content.add(panNumber);
		 content.add(panGeneration);
		 content.add(panGene);
		 content.add(panCreation);
		 content.add(panMutation);
		 content.add(panCrossover);		 
		 content.add(panSelection);
	    
		 JPanel control = new JPanel();
		 JButton okBouton = new JButton("OK");
		 
		 JButton cancelBouton = new JButton("Annuler");
		 cancelBouton.addActionListener(new ActionListener()
		 {
		 public void actionPerformed(ActionEvent arg0)
		 {
		 setVisible(false);
		 }
		 });
			
		 control.add(okBouton);
		 control.add(cancelBouton);
		     
		 okBouton.addActionListener(new ActionListener()
		 	{
			    public void actionPerformed(ActionEvent arg0) 
			    {   
			    	boolean error = false;
			    	int i = 0;
				    /*Case : value enter is not a digit*/			    	
				    while(i<number.getText().length() && error != true)
				    {
					   
				    	if(Character.isDigit(number.getText().charAt(i))==false)
				    	{
				    		error = true;
				    	    JOptionPane.showMessageDialog(null, "The number of chromosome is not a digit", "Error", JOptionPane.ERROR_MESSAGE); 
				    	}
				    	i++;
				    }
				    /*case nothing is enter*/
				    if(number.getText().equals("") && error!=true)
				    {
				    	error = true;
				    	JOptionPane.showMessageDialog(null, "Number of chromosome is missing", "Error", JOptionPane.ERROR_MESSAGE); 
				    }
				    i=0;
				    
				    while(i<generation.getText().length() && error != true)
				    {
				    	if(Character.isDigit(generation.getText().charAt(i))==false)
				    	{
				    		error = true;
					   		JOptionPane.showMessageDialog(null, "The numberOfGeneration is not a digit", "Error", JOptionPane.ERROR_MESSAGE); 
				    	}
				    	i++;
				    }
				    if(generation.getText().equals("") && error!=true)
				    {
				    	error = true;
				    	JOptionPane.showMessageDialog(null, "Number of generation is missing", "Error", JOptionPane.ERROR_MESSAGE); 
				    }
				    
				    while(i<length.getText().length() && error != true)
				    {
				    	if(Character.isDigit(length.getText().charAt(i))==false)
				    	{   
				    		error = true;				    		
					   		JOptionPane.showMessageDialog(null, "The Chromosome length is not a digit", "Error", JOptionPane.ERROR_MESSAGE); 
				    	}
				    }
				    if(length.getText().equals("") && error!=true)
				    {
				    	error = true;
				    	JOptionPane.showMessageDialog(null, "Chromosome length is missing", "Error", JOptionPane.ERROR_MESSAGE);
				    }		
				    /*case chromosome length is to high*/
				    if(Integer.valueOf(length.getText())>10 && error!= true)
				    {
				    	error = true;
				    	JOptionPane.showMessageDialog(null, "Chromosome length must be included between 1 and 10", "Error", JOptionPane.ERROR_MESSAGE);
				    }
				    if(error == false)
				    {
				    	confWindInf = new ConfigurationWindowInfo(getboolCreation(), getboolMutation(), getboolCrossover(), getboolGene(), getNumberOfChromosome(), getChromosomeLength(), getNumberOfGeneration(), getNumberSelection());
				    	setVisible(false);	
				    	confOK = true;
				    }
			    }
			    
			    public boolean getboolCreation()
			    {
			    	if (choiceCreation2.isSelected())
			    	{
			    		return true;
			    	}
			    	else 
			    	{
			    		return false;
			    	}
			    }
			    
			    public boolean getboolMutation()
			    {
			    	if (choiceMutation2.isSelected())
			    	{
			    		return true;
			    	}
			    	else 
			    	{
			    		return false;
			    	}	
			    }
			    
			    public boolean getboolCrossover()
			    {
			    	if (choiceCrossover2.isSelected())
			    	{
			    		return true;
			    	}
			    	else 
			    	{
			    		return false;
			    	}	
			    }
			    
			    public boolean getboolGene()
			    {
			    	if (choiceGene2.isSelected())
			    	{
			    		return true;
			    	}
			    	else 
			    	{
			    		return false;
			    	}	
			    }
			    
			    public int getNumberOfChromosome()
			    {
			    	return Integer.valueOf(number.getText());
			    }
			    
			    public int getNumberOfGeneration()
			    {
			    	return Integer.valueOf(generation.getText());
			    }
			    
			    public int getChromosomeLength()
			    {
			    	return Integer.valueOf(length.getText());
			    }
			    
			    public int getNumberSelection()
			    {
			    	if(choiceSelection1.isSelected())
			    	{
			    		return 0;
			    	}
			    	else if(choiceSelection2.isSelected())
			    	{
			    		return 1;
			    	}	
			    	else if(choiceSelection3.isSelected())
			    	{
			    		return 2;
			    	}
			    	else
			    	{
			    		return 3;
			    	}
			    }
		    }); 		
		 
		    this.getContentPane().add(content,BorderLayout.CENTER);		    
		    this.getContentPane().add(control,BorderLayout.SOUTH);
		    this.setVisible(true);
	 }
	 
	/**
	 * Get the boolean of confirmation
	 * 
	 * @return the boolean of confirmation
	 */
	 public boolean getConfOK()
	 {
		 return confOK;
	 }
	 
	 /**
	  * Get the object with the information of the configuration
	  * 
	  * @return the object of configuration
	  * 
	  * @see ConfigurationWindowInfo
	  */
	 public ConfigurationWindowInfo getConfigurationWindowInfo()
	 {
		 return confWindInf;
	 }
}

