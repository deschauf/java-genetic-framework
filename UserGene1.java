package pack;
public class UserGene1 implements IGene
{
	private boolean bo_allel;
	private int i_allel;
	private double d_allel;
	private long l_allel;
	private float f_allel;
	private short s_allel;
	private byte by_allel;

	/*
	 * Constructor 
	 */
	
	public UserGene1()
	{
	}
	
	/*
	 *Function set
	 */
	
	public void setAllel(Object value, Configuration conf)
	{		
		switch (conf.getGeneType())
		{
		case 1 :
			bo_allel = (Boolean)(value);
		break;
		case 2 : 
			i_allel = (Integer)(value);
		break;
		case 3 :
			d_allel = (Double)(value);
		break;
		case 4 :
			l_allel = (Long)(value);
		break;
		case 5 :
			f_allel = (Float)(value);
		break;
		case 6 :
			s_allel = (Short)(value);
		break;
		case 7 :
			by_allel = (Byte)(value);
		break;
		}
	}
	
	/*
	 * Function get
	 */	
	
	public Object getAllel(Configuration conf)
	{
		switch (conf.getGeneType())
		{
		case 1 :
			return bo_allel;
		case 2 : 
		 	return i_allel;
		case 3 :
			return d_allel;
		case 4 :
			return l_allel;
		case 5 :
			return f_allel;
		case 6 :
			return s_allel;
		case 7 :
		 	return by_allel;
		 default :
			 System.out.println("The type of Gene is incorrect");
			 return bo_allel;
		}
	}
	
	public Object getAllel(int value)
	{
		switch (value)
		{
		case 1 :
			return bo_allel;
		case 2 : 
		 	return i_allel;
		case 3 :
			return d_allel;
		case 4 :
			return l_allel;
		case 5 :
			return f_allel;
		case 6 :
			return s_allel;
		case 7 :
		 	return by_allel;
		 default :
			 System.out.println("The type of Gene is incorrect");
			 return bo_allel;
		}
	}
}
