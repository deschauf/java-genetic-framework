/*
 *     Copyright (C) <2013>  <DESCHAU Flavien VASSAL Adrien>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package pack;
public class Configuration 
{
	/**
	 * The number of chromosome in the population
	 * 
	 * @see Chromosome
	 */
	private int numberOfChromosome;
	
	
	/**
	 * The length of a chromosome
	 * 
	 * @see Chromomsome
	 */
	private int chromosomeLength;
	
	/**
	 * The number of generation during the run
	 */
	private int numberOfGeneration;
	
	/**
	 * The type used for the genes
	 * 
	 * @see Gene
	 * @see DefaultGene
	 */
	private int geneType;
	
	/**
	 * The lower bound of the Gene value
	 * <p> 
	 * This variable is only used when the gene is not a boolean
	 * <p>
	 */
	private int borneInf;
	
	/**
	 * The upper bound of the Gene value
	 * <p> 
	 * This variable is only used when the gene is not a boolean
	 * <p>
	 */
	private int borneSup;
	
	/**
	 * A table of integer which represent the construction of the chromosome
	 * <p>
	 * This table is only used when the user create his own Gene
	 * <p>
	 */
	private int [] chromosomeConfig;
	
	/**
	 * The operator of mutation to use in the program
	 * 
	 *  @see IMutateOperator
	 *  @see MutateOperator
	 */
	private Object mutationConf = new Object();
	
	/**
	 * The operator of crossover to use in the program
	 * 
	 * @see ICrossoverOperator
	 * @see CrossoverOperator
	 */
	private Object crossoverConf = new Object();
	
	/**
	 * The operator of creation to use in the program
	 * 
	 * @see ICreateperator
	 * @see CerateOperator
	 */
	private Object createConf = new Object();
	
	/**
	 * The operator of print to use in the program
	 * 
	 * @see IPrintOperator
	 * @see PrintOperator
	 */
	private Object printPopConf = new Object();
	
	/**
	 * The operator of selection to use in the program
	 * 
	 * @see ISelectionOperator
	 * @see SelectionOperator
	 */
	private Object selectionConf = new Object();
	
	/**
	 * The operator of fitness to use in the program
	 * 
	 * @see FitnessFunction
	 */
	private Object fitnessConf = new Object();
	
	/**
	 * Boolean which represent the choice to print in a file or not
	 */
	private boolean printInFile = false;
		
	/**
	 * Default constructor of configuration
	 * <p>
	 * All the operator are set to default operator
	 * <p>
	 */
	public Configuration()
	{
		chromosomeLength = 0;
		numberOfChromosome = 0;
		numberOfGeneration = 0;
		geneType=1;
		chromosomeConfig = new int[0];
		mutationConf = new MutateOperator();
		crossoverConf = new CrossoverOperator();
		createConf = new CreateOperator();
		printPopConf = new PrintOperator();
		selectionConf = new SelectionTournamentProbability();
		fitnessConf = new Fitness();
	}
	/**
	 * Overloading of the constructor 
	 * <p>
	 * All the operator are set to default operator
	 * <p>
	 * @param length 
	 * 			represent the length of the chromosome	
	 * @param number
	 * 			represent the number of chromosome in the population
	 * @param actualNumberOfGeneration
	 * 			represent the number of generation for the run
	 */
	public Configuration(int length, int number, int actualNumberOfGeneration)
	{
		chromosomeLength = length;
		numberOfChromosome = number;
		numberOfGeneration = actualNumberOfGeneration;
		geneType=1;
		chromosomeConfig = new int[0];
		mutationConf = new MutateOperator();
		crossoverConf = new CrossoverOperator();
		createConf = new CreateOperator();
		printPopConf = new PrintOperator();
		selectionConf = new SelectionTournamentProbability();
		fitnessConf = new Fitness();
	}
	
	/**
	 * Set the choice to print in file
	 * 
	 * @param value
	 * 			boolean which represent the choice of the user
	 */
	public void setPrintInFile(boolean value)
	{
		printInFile=value;
	}
	
	/**
	 * Set the length of the chromosome
	 * 
	 * @param value
	 * 			the value of the length
	 */
	public void setChromosomeLength(int value)
	{
		chromosomeLength = value;
	}
	
	/**
	 * Set the number of chromosome
	 * 
	 * @param value
	 * 			the value of the number of chromosome
	 */	
	public void setNumberOfChromosome(int value)
	{
		numberOfChromosome = value;
	}
	
	/**
	 * Set the number of generation
	 * 
	 * @param value
	 * 			the value of the number of generation
	 */
	public void setNumberOfGeneration(int value)
	{
		numberOfGeneration = value;
	}
	
	/**
	 * Set the mutation operator
	 * 
	 * @param value
	 * 			The mutation operator that the user want to used
	 * 
	 * @see MutateOperator
	 */	
	public void setMutationOperator(Object value)
	{
		mutationConf = value;
	}
	
	/**
	 * Set the crossover operator
	 * 
	 * @param value
	 * 			The crossover operator that the user want to used
	 * 
	 * @see CrossoverOperator
	 */
	public void setCrossoverOperator(Object value)
	{
		crossoverConf = value;
	}
	
	/**
	 * Set the creator operator
	 * 
	 * @param value
	 * 			The creator operator that the user want to used
	 * 
	 * @see CreateOperator
	 */
	public void setCreateOperator(Object value)
	{
		createConf = value;
	}
	
	/**
	 * Set the print operator
	 * 
	 * @param value
	 * 			The print operator that the user want to used
	 * 
	 * @see PrintOperator
	 */
	public void setPrintPopOperator(Object value)
	{
		printPopConf = value;
	}

	/**
	 * Set the selection operator
	 * 
	 * @param value
	 * 			The selection operator that the user want to used
	 * 
	 * @see SelectionTournamentProbability
	 */
	public void setSelectionOperator(Object value)
	{
		selectionConf = value;
	}
	
	/**
	 * Set the fitness operator
	 * 
	 * @param value
	 * 			The fitness operator that the user want to used
	 * 
	 */
	public void setFitnessOperator(Object value)
	{
		fitnessConf = value;
	}
	
	/**
	 * Set the construction of the chromosome
	 * 
	 * @param value
	 * 			table of integer which represent the construction of the chromosome
	 * 
	 * @see Chromosome
	 */
	public void setChromosomeConfig(int[] value)
	{
		chromosomeConfig = new int[value.length];
		chromosomeConfig = value;
	}
	
	/**
	 * set the construction of the chromosome
	 * 
	 */	
	public void setChromosomeConfig()
	{
		chromosomeConfig = new int[0];
	}
	
	/**
	 * Set the type of the gene
	 * 
	 * @param value
	 * 			An integer which represent a type
	 * 
	 * @see Gene
	 */
	public void setGeneType(int value)
	{
		geneType = value;
	}
	
	/**
	 * Set the interval for the value of the gene
	 * <p>
	 * This function is used when the type of the gene is not a boolean
	 * <p>
	 * 
	 * @param value1
	 * 			The lower bound
	 * @param value2
	 * 			The upper bound
	 * 
	 * @see DefaultGene
	 */
	public void setInterval(int value1, int value2)
	{
		borneInf=value1;
		borneSup=value2;
	}
	
	/**
	 * Get the print boolean
	 * <p>
	 * This boolean represent the right to print in file or not
	 * <p>
	 * 
	 * @return The boolean 
	 */
	public boolean getPrintInFile()
	{
		return printInFile;
	}

	public int getNumberOfChromosome()
	{
		return numberOfChromosome;
	}
	
	public int getChromosomeLength()
	{
		return chromosomeLength;
	}
	
	public Object getCrossoverOperator()
	{
		return crossoverConf;
	}
	
	public Object getMutationOperator()
	{
		return mutationConf;
	}
	
	public Object getCreateOperator()
	{
		return createConf;
	}
	
	public Object getPrintPopOperator()
	{
		return printPopConf;
	}

	public Object getSelectionOperator()
	{
		return selectionConf;
	}
	
	public Object getFitnessOperator()
	{
		return fitnessConf;
	}		
	
	int getNumberOfGeneration()
	{
		return numberOfGeneration;
	}
	
	int[] getChromosomeConfig()
	{
		return chromosomeConfig;
	}
	
	int getGeneType()
	{
		return geneType;
	}
	
	int getBorneInf()
	{
		return borneInf;
	}
	
	int getBorneSup()	
	{
		return borneSup;
	}
}
