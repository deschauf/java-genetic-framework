/*
 *     Copyright (C) <2013>  <DESCHAU Flavien VASSAL Adrien>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package pack;

public class Point 
{
	/**
	 * Table of two int which represent the X and Y position of a point
	 */
	int position[] = new int[2];
	
	/**
	 * Actual fitness scale
	 * <p>
	 * This is useful for the graphic construction
	 * <p>
	 */
	int scaleFit;
	
	/**
	 * Actual generation scale
	 * <p>
	 * This is useful for the graphic construction
	 * <p>
	 */
	int scaleGeneration;
	
	/**
	 * Actual fitness of a chromosome
	 */
	int fit;
	
	/**
	 * Actual number of generation
	 */
	int gen;

	public Point()
	{		
	}

	/**
	 * Initialization of the point
	 * @param actualFitnessScale
	 * @param actualGenerationScale
	 */
	public Point(int actualFitnessScale, int actualGenerationScale)
		{
			scaleFit=actualFitnessScale; 
			scaleGeneration=actualGenerationScale;
		}
	
		/*
		 * Function Set
		 */
		
		public void setScaleGene(int value)
		{
			scaleGeneration=value;
		}
		
		public void setScaleFit(int value)
		{
			scaleFit=value;
		}
		
		public void setPosition(int value, int index)
		{
			position[index]=value;
		}
		
		public void setFit(int value)
		{
			fit=value;
		}
				
		public void setGen(int value)
		{
			gen=value;
		}
		
		/*
		 * Function Get
		 */
		
		public int getGen()
		{
			return gen;
		}
		
		public int getFit()
		{
			return fit;
		}	
		
		public int getScaleGene()
		{
			return scaleGeneration;
		}
		
		public int getScaleFit()
		{
			return scaleFit;
		}
}

