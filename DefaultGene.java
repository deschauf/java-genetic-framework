/*
 *     Copyright (C) <2013>  <DESCHAU Flavien VASSAL Adrien>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package pack;
public class DefaultGene implements IGene
{
	/**
	 * Allele of boolean type
	 */
	private boolean bo_allel;
	
	/**
	 * Allele of integer type
	 */
	private int i_allel;
	
	/**
	 * Allele of double type
	 */
	private double d_allel;
	
	/**
	 * Allele of long type
	 */
	private long l_allel;
	
	/**
	 * Allele of float type
	 */
	private float f_allel;

	/**
	 * Constructor
	 */
	public DefaultGene()
	{
	}
	
	/*
	 *Function set
	 */
	
	public void setAllel(Object value, Configuration conf)
	{		
		switch (conf.getGeneType())
		{
		case 1 :
			bo_allel = (Boolean)(value);
		break;
		case 2 : 
			i_allel = (Integer)(value);
		break;
		case 3 :
			d_allel = (Double)(value);
		break;
		case 4 :
			l_allel = (Long)(value);
		break;
		case 5 :
			f_allel = (Float)(value);
		break;		
		}
	}
	
	/*
	 * Function get
	 */	
	
	public Object getAllel(Configuration conf)
	{
		switch (conf.getGeneType())
		{
		case 1 :
			return bo_allel;
		case 2 : 
		 	return i_allel;
		case 3 :
			return d_allel;
		case 4 :
			return l_allel;
		case 5 :
			return f_allel;
		 default :
			 System.out.println("The type of Gene is incorrect");
			 return bo_allel;
		}
	}
	
	public Object getAllel(int value)
	{
		switch (value)
		{
		case 1 :
			return bo_allel;
		case 2 : 
		 	return i_allel;
		case 3 :
			return d_allel;
		case 4 :
			return l_allel;
		case 5 :
			return f_allel;
		 default :
			 System.out.println("The type of Gene is incorrect");
			 return bo_allel;
		}
	}
}
