/*
 *     Copyright (C) <2013>  <DESCHAU Flavien VASSAL Adrien>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package pack;
import java.util.Random;


public class SelectionBiasedWheel implements ISelectionOperator  
{

	/**
	 * Operator of selection : biased wheel
	 * 
	 * @param actualPopulation
	 * @param conf
	 */
	public Chromosome[] selection (Chromosome[] actualPopulation, Configuration conf)
	{
		int numberOfChromosome= conf.getNumberOfChromosome();
	    Random rnd = new Random();
	    int j = 0,sum=0;
	    int tab[] = new int[actualPopulation.length];
	    Chromosome[] wheel = new Chromosome[numberOfChromosome];
	    for(int i=0; i< actualPopulation.length;i++)
	    {
	    	if(actualPopulation[i].getChromosomeFitness()>=0)
	    	{
		    	sum+= actualPopulation[i].getChromosomeFitness();
		    	tab[i]= sum;
	    	}
	    	else
	    	{
	    		sum+=1;
	    		tab[i]=sum;
	    	}
	    }
	    for (int k=0;k<numberOfChromosome;k++)
		{
	    	wheel[k]= new Chromosome(conf);
	    	int value=rnd.nextInt(sum-tab[0])+tab[0];
	    	while(value<tab[j])
	    	{
	    		j++;
	    	}
	    	wheel[k]=actualPopulation[j];
	    	j=0;
	    }
	    return wheel;
	}
}

