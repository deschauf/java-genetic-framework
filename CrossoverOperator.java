/*
 *     Copyright (C) <2013>  <DESCHAU Flavien VASSAL Adrien>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package pack;
import java.util.Random;


public class CrossoverOperator implements ICrossoverOperator
{
	
	/**
	 * Realized a crossover of solution in a given population
	 * 
	 * @param conf
	 * 		A configuration object
	 * @param myPopulation
	 * 		A population of solutions
	 * @return crossoverPopulation
	 * 		A table of chromosome which represent the crossover population
	 * 
	 * @see ICrossoverOperator
	 */
	
	public Chromosome [] crossover(Configuration conf, Chromosome myPopulation[])
	{
		int numberOfChromosome = conf.getNumberOfChromosome();
		int chromosomeLength = conf.getChromosomeLength();
		int index[] = new int[numberOfChromosome];
		int crossPoint, random;
		Chromosome chromosomeSon1;
		Chromosome chromosomeSon2;
		Chromosome crossoverPopulation[];
		Chromosome actualChromosome = new Chromosome(conf);
		Random rnd = new Random();
		int numberOfCrossover=0;
				
		/*
		 * Determine the number of mutation
		 */
		
		for(int i=0; i<numberOfChromosome;i++)
		{
			if(rnd.nextInt(9)>=2)
			{
				numberOfCrossover++;
			}
		}
	
		/*
		 * Maximize the number of mutation
		 */
		
		if(numberOfChromosome%2 == 0)
		{
			if(numberOfCrossover > (numberOfChromosome/2))
			{
				numberOfCrossover = numberOfChromosome/2;
			}
		}
		if(numberOfChromosome%2 != 0)
		{
			if(numberOfCrossover > ((numberOfChromosome-1)/2))
			{
				numberOfCrossover = (numberOfChromosome-1)/2;
			}
		}		
		if(chromosomeLength == 1)
		{
			numberOfCrossover = 0;
		}		

		/*
		 * Initialize the table of Chromosome
		 */
		
		crossoverPopulation = new Chromosome[2*numberOfCrossover];
		
		for(int i=0; i<2*numberOfCrossover;i++)
		{
			crossoverPopulation[i] = new Chromosome(conf);
		}		
		
		/*
		 * Initialize the table of index (In order to know which chromosome has ever
		 * been used for a crossover )
		 */
		
		for(int i=0; i<numberOfChromosome;i++)
		{
			index[i]=1;
		}		
	
		/*
		 * Beginning of the crossover operation
		 */
		
		for(int i=0; i<numberOfCrossover ;i++)
		{					
			index[i]=0;				
			do
			{
			random = rnd.nextInt(numberOfChromosome-i);	
			actualChromosome = myPopulation[i+random];
			}
			while (index[i+random] == 0);	
			
			index[random+i] = 0;
			crossPoint = rnd.nextInt(chromosomeLength);		
			chromosomeSon1 = new Chromosome(conf);
			chromosomeSon2 = new Chromosome(conf);
			for(int j=0;j<chromosomeLength;j++)	
			{						
				if(j<=crossPoint)
				{
					chromosomeSon1.setGeneAt(j,actualChromosome.getGeneAt(j));
					chromosomeSon2.setGeneAt(j,myPopulation[i].getGeneAt(j));
				}
				if(j>crossPoint)
				{
					chromosomeSon1.setGeneAt(j,myPopulation[i].getGeneAt(j));
					chromosomeSon2.setGeneAt(j,actualChromosome.getGeneAt(j));
				}				
			}
			crossoverPopulation[i].setChromosomeList(chromosomeSon1.getChromosomeList());
			crossoverPopulation[crossoverPopulation.length-i-1].setChromosomeList(chromosomeSon2.getChromosomeList());
		}
	return crossoverPopulation;
	}
}
