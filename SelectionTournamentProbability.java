/*
 *     Copyright (C) <2013>  <DESCHAU Flavien VASSAL Adrien>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package pack;

import java.util.Random;

public class SelectionTournamentProbability implements ISelectionOperator
{
	
	/**
	 * Operator of selection : Selection tournament with probability
	 * <p>
	 * This is the default selection operator
	 * <p>
	 * 
	 * @param actualPopulation
	 * @param conf
	 */
	
	public Chromosome[] selection (Chromosome[] actualPopulation, Configuration conf)
	{
		int numberOfChromosome = conf.getNumberOfChromosome();		
		Random rnd = new Random();
		Random rnd2 = new Random();
		Random select = new Random();
		int s;
		Chromosome tournament[] = new Chromosome[numberOfChromosome];
		
		for(int i=0;i<numberOfChromosome;i++)
		{
			tournament[i]=new Chromosome(conf);
			s = select.nextInt(100);
			int r=rnd.nextInt(actualPopulation.length);
			int r2=rnd2.nextInt(actualPopulation.length);
			if((actualPopulation[r].getChromosomeFitness()<actualPopulation[r2].getChromosomeFitness())&&(s>=25)&&(actualPopulation[r2].getChromosomeFitness()>=0))
			{
				tournament[i]=actualPopulation[r2];

			}
			else if (actualPopulation[r].getChromosomeFitness()>=0)
			{
				tournament[i]=actualPopulation[r];

			}
			else
			{
				actualPopulation[r].setChromosomeFitness(0);
				actualPopulation[r2].setChromosomeFitness(0);
				tournament[i]=actualPopulation[r];
			}
			
		}
		return tournament;
	}
	
}
