/*
 *     Copyright (C) <2013>  <DESCHAU Flavien VASSAL Adrien>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package pack;

import java.awt.*;

import javax.swing.JPanel;
import java.awt.Color;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionListener;

import java.util.List;

public class Panneau extends JPanel implements MouseMotionListener

{ 		
	private static final long serialVersionUID = 1L;
	
	/**
	 * Object which calculate the position of the point on the graphic
	 */
	private PointCalculator myPointCalculator;	
	private int myNumberOfGeneration;
	private int mousePosX = 0;
	private int mousePosY = 0;
	private int actualGenerationScale = 10;
	private int actualFitnessScale = 10;
	
	public Panneau()
	{		
	}	
	
	/**
	 * Actualize the data of the points to print
	 * 
	 * @param actualPointCalculator
	 * @param actualNumberOfGeneration
	 * @param actualMousePosX
	 * @param actualMousePosY
	 * 
	 * @see PointCalculator
	 */
	
	public Panneau(PointCalculator actualPointCalculator, int actualNumberOfGeneration, int actualMousePosX,int actualMousePosY)
	{
		myPointCalculator = actualPointCalculator;
		myNumberOfGeneration = actualNumberOfGeneration;
		addMouseMotionListener( this );
		mousePosX=actualMousePosX;
		mousePosY=actualMousePosY;
		actualGenerationScale = actualPointCalculator.getActualGenerationScale();
		actualFitnessScale = actualPointCalculator.getActualFitnessScale();
	}
	
	/**
	 * Print the graphic 
	 * 
	 * @param g
	 * 		A graphical object
	 */
	
	public void paintComponent(Graphics g)
	{	
		legend(g);
		g.setColor(new Color(50,160,50));		
		g.drawLine(this.getWidth()-85, 55, this.getWidth()-60, 55);
		drawLine2point(g, myPointCalculator.getPointToPrintMax());
		g.setColor(Color.red);		
		g.drawLine(this.getWidth()-85, 70, this.getWidth()-60, 70);
		drawLine2point(g, myPointCalculator.getPointToPrintAvg());	
	}
	
	/**
	 * Draw a line between two give point
	 * @param g
	 * 		A graphical object
	 * @param Line
	 * 		A list of point 
	 */
	
	void drawLine2point(Graphics g, List<Point> Line )
	{		
		for(int i=0;i<Line.size()-1;i++)
		{	
			g.drawLine(Line.get(i).position[0],Line.get(i).position[1],Line.get(i+1).position[0],Line.get(i+1).position[1]);			
		}
	}	
	
	/**
	 * Creation of the legend of the graphic
	 * 
	 * @param g
	 * 		A graphical object
	 */
	void legend(Graphics g)
	{
		Graphics2D squareGraphic = (Graphics2D)g;
		/*background*/
		g.setColor(new Color(40, 196, 36));
		g.fillRect(0, 0, getWidth(), getHeight());
		/*Legend graphical object*/
		g.setColor(new Color(32, 70, 223));
		g.fillRoundRect(40+myPointCalculator.getGraphicalWidth()+30,40,180,50,20,20);
		g.fillRoundRect(40+myPointCalculator.getGraphicalWidth()+30, 130, 180, 60, 20, 20);
		g.fillRoundRect(40+myPointCalculator.getGraphicalWidth()+30, 230, 180, 60, 20, 20);
		g.fillRoundRect(40+myPointCalculator.getGraphicalWidth()+30, 330, 180, 60, 20, 20);
		g.fillRoundRect(40+myPointCalculator.getGraphicalWidth()+30, 430, 180, 60, 20, 20);
		g.fillRoundRect(40+myPointCalculator.getGraphicalWidth()+30, 530, 180, 80, 20, 20);
		/*grid background*/
		squareGraphic.setColor(Color.WHITE);
		g.fillRect(40,40,getWidth()-280,getHeight()-80);
		/*Loading bar*/
		g.fillRect(40+myPointCalculator.getGraphicalWidth()+35, 460,(int)((float)((float)(myPointCalculator.getNumberOfGeneration())/(float)(myNumberOfGeneration))*170) , 20);
		/*Square outline*/
		squareGraphic.setColor(Color.BLACK);
		g.drawRoundRect(40+myPointCalculator.getGraphicalWidth()+30,40,180,50,20,20);
		g.drawRoundRect(40+myPointCalculator.getGraphicalWidth()+30, 130, 180, 60, 20, 20);
		g.drawRoundRect(40+myPointCalculator.getGraphicalWidth()+30, 230, 180, 60, 20, 20);
		g.drawRoundRect(40+myPointCalculator.getGraphicalWidth()+30, 330, 180, 60, 20, 20);
		g.drawRoundRect(40+myPointCalculator.getGraphicalWidth()+30, 430, 180, 60, 20, 20);
		g.drawRoundRect(40+myPointCalculator.getGraphicalWidth()+30, 530, 180, 80, 20, 20);
		g.drawRect(40+myPointCalculator.getGraphicalWidth()+35, 460, 170, 20);			
		/*Value of actual fitness average*/
		if(!myPointCalculator.getPointToPrintAvg().isEmpty())
		{
			g.drawString(String.valueOf(myPointCalculator.getPointToPrintAvg().get(myPointCalculator.getPointToPrintAvg().size()-1).getFit()), 40+myPointCalculator.getGraphicalWidth()+100, 180);
		}
		/*Best Chromosome*/
		if((myPointCalculator.getConfig().getChromosomeLength()<5)||myPointCalculator.getConfig().getGeneType()==1)
		{
			g.drawString(myPointCalculator.ChrToString(), 40+myPointCalculator.getGraphicalWidth()+50, 380);
		}
		else
		{
			int save=0;
			for(int i = 0;i<myPointCalculator.ChrToString().length()/2;i++)
			{
				if(myPointCalculator.ChrToString().charAt(i)==' ')
				{
					save = i;
				}
			}
			g.drawString(myPointCalculator.ChrToString().substring(0,save), 40+myPointCalculator.getGraphicalWidth()+50, 370);
			g.drawString(myPointCalculator.ChrToString().substring(save, myPointCalculator.ChrToString().length()-1), 40+myPointCalculator.getGraphicalWidth()+50, 380);
		}
		/*Value of actual fitness max*/
		if(!myPointCalculator.getPointToPrintMax().isEmpty())
		{
			g.drawString(String.valueOf(myPointCalculator.getPointToPrintMax().get(myPointCalculator.getPointToPrintMax().size()-1).getFit()), 40+myPointCalculator.getGraphicalWidth()+100, 280);
		}	
		/*Number Of generation*/
		g.drawString(String.valueOf(myPointCalculator.getNumberOfGeneration()), 40+myPointCalculator.getGraphicalWidth()+110, 475);
		/*Position of the cursor*/
		if(mousePosX>=0 && mousePosX<=actualGenerationScale*10 && mousePosY>=0 && mousePosY<=actualFitnessScale*10-20) //offset due to the
		{
			g.drawString(String.valueOf(mousePosY), 40+myPointCalculator.getGraphicalWidth()+120, 560);
			g.drawString(String.valueOf(mousePosX), 40+myPointCalculator.getGraphicalWidth()+120, 590);
		}		
		/*Legend text*/
		Font font = new Font("Courrier",Font.BOLD,12);
		g.setFont(font);		
		g.drawString("actual average", 40+myPointCalculator.getGraphicalWidth()+75, 145);
		g.drawString("actual max", 40+myPointCalculator.getGraphicalWidth()+90, 245);
		g.drawString("number of generation", 40+myPointCalculator.getGraphicalWidth()+60,445);
		g.drawString("Maximum Fitness",40+myPointCalculator.getGraphicalWidth()+50,60);
		g.drawString("Average Fitness",40+myPointCalculator.getGraphicalWidth()+50,75);
		g.drawString("Number of génération", this.getWidth()-360, this.getHeight()-10);
		g.drawString("Fitness Value", 15, 20);
		g.drawString("Fitness : ",40+myPointCalculator.getGraphicalWidth()+35, 560);
		g.drawString("Generation : ", 40+myPointCalculator.getGraphicalWidth()+35, 590);
		g.drawString("Best Solution : ",40+myPointCalculator.getGraphicalWidth()+80, 345);
		/*grid*/
		AxeX(g);
		AxeY(g);
	}

	/**
	 * Creation of the X axe
	 * @param g
	 *		A graphical object
	 */
	void AxeX(Graphics g)
	{
		int x = myPointCalculator.getActualGenerationScale();
		g.setColor(Color.black);
		g.drawLine(40, this.getHeight()-40, this.getWidth()-240, this.getHeight()-40);
		g.drawLine(this.getWidth()-245, this.getHeight()-35, this.getWidth()-240, this.getHeight()-40);
		g.drawLine(this.getWidth()-245, this.getHeight()-45, this.getWidth()-240, this.getHeight()-40);
		
		for(int i = 0; i<10; i++)
		{
			g.setColor(Color.GRAY);
			g.drawLine((int)(i*(myPointCalculator.getGraphicalWidth())/10)+40,this.getHeight()-40,(int)(i*(myPointCalculator.getGraphicalWidth())/10)+40,40);	
			g.setColor(Color.BLACK);
			g.drawString(String.valueOf(i*x),(int)(i*(myPointCalculator.getGraphicalWidth())/10)+40,this.getHeight()-25);
		}
	}
	
	/**
	 * Creation of the Y axe
	 * 
	 * @param g
	 *		A graphical object
	 */	
	void AxeY(Graphics g)
	{
		int y = myPointCalculator.getActualFitnessScale();
		g.setColor(Color.black);
		g.drawLine(40,this.getHeight()-40,40,40);
		g.drawLine(35,45,40,40);
		g.drawLine(45,45,40,40);
		
		for(int i = 1;i<10;i++)
		{
			g.setColor(Color.GRAY);
			g.drawLine(40,this.getHeight()-i*(myPointCalculator.getGraphicalHeight())/10-40,this.getWidth()-240, this.getHeight()-i*(myPointCalculator.getGraphicalHeight())/10-40);	
			g.setColor(Color.BLACK);
			g.drawString(String.valueOf(i*y),10,this.getHeight()-i*(myPointCalculator.getGraphicalHeight())/10-40);	
		}
	}
	
	/**
	 * Get the actual position of the cursor
	 */
	public void mouseDragged(MouseEvent arg0) {		
	}

	public void mouseMoved(MouseEvent e) 
	{
		mousePosX=0;
		mousePosY=0;
		if((e.getX()-40)*10/(myPointCalculator.getGraphicalWidth()) != 0)
		{		
			mousePosX = (e.getX()-40)*10/(myPointCalculator.getGraphicalWidth())*actualGenerationScale;
			mousePosX += ((e.getX()-40)%((myPointCalculator.getGraphicalWidth())/10))*actualGenerationScale/((myPointCalculator.getGraphicalWidth())/10);
		}		
		else
		{
			mousePosX +=((e.getX()-40)*actualGenerationScale/((myPointCalculator.getGraphicalWidth())/10));
		}		
		if((this.getHeight()-e.getY()-40)*10/((myPointCalculator.getGraphicalHeight())) != 0)
		{
			mousePosY = (this.getHeight()-e.getY()-40)/((myPointCalculator.getGraphicalHeight())/10)*actualFitnessScale;
			mousePosY += (this.getHeight()-e.getY()-40)%((myPointCalculator.getGraphicalHeight())/10)*actualFitnessScale/((myPointCalculator.getGraphicalHeight())/10);
		}		
		else
		{
			mousePosY += (this.getHeight()-e.getY()-40)*actualFitnessScale/((myPointCalculator.getGraphicalHeight())/10);
		}	
	}
	
	public int getMousePosX()
	{
		return mousePosX;
	}
	
	public int getMousePosY()
	{
		return mousePosY;
	}
	
}
	