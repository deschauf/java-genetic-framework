/*
 *     Copyright (C) <2013>  <DESCHAU Flavien VASSAL Adrien>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package pack;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;

public class Window extends JFrame
{
	private static final long serialVersionUID = 1L;
	
	/**
	 * Position X of the mouse
	 */
	private int mousePosX=0;
	
	/**
	 * Position Y of the mouse
	 */
	private int mousePosY=0;
	
	/**
	 * Validation boolean
	 * <p>
	 * The value is true when the configuration is correct
	 * <p>
	 */
	private int controleValue=0;
	
	/**
	 * Initialize from a file or not
	 */
	private boolean file = false;
	
	/**
	 * The simulation is launch or not
	 */
	private boolean launch=false;
	
	/**
	 * The simulation is paused or not
	 */
	private boolean pause=false;
	
	/**
	 * Graphical panel
	 */
	private Panneau pan;

	/**
	 * File to print the result in
	 */
	private File result;
	
	/**
	 * Window of configuration
	 * 
	 * @see ConfigurationWindow
	 */
	private ConfigurationWindow confWind = new ConfigurationWindow();
	
	/**
	 * Graphical object JMenueBar
	 */
	private JMenuBar menuBar = new JMenuBar();
	
	/**
	 * Graphical object 'File' button
	 */
	private JMenu test1 = new JMenu("File");
	
	/**
	 * Graphical object 'Edition' button
	 */
	private JMenu test2 = new JMenu("Edition");		
	
	/* Creation of the item which take part of our drop down */	
	
	/**
	 * Graphical object 'open' button
	 */
	private JMenuItem item1 = new JMenuItem("Open");
	
	/**
	 * Graphical object 'Stop' button
	 */
	private JMenuItem item2 = new JMenuItem("Stop");
	
	/**
	 * Graphical object 'Launch' button
	 */
	private JMenuItem item3 = new JMenuItem("Launch");
	
	/**
	 * Graphical object 'Pause' button
	 */
	private JMenuItem item4 = new JMenuItem("Pause");
	
	/**
	 * Graphical object 'Start' button
	 */
	private JMenuItem item5 = new JMenuItem("Start");
	
	/**
	 * Graphical object 'Configuration' button
	 */
	private JMenuItem item6 = new JMenuItem("Configuration");	
	
	public Window()
	{
	}	
	/**
	 * Initialization of the frame
	 * @param Height
	 * 		Height of the frame
	 * @param Width
	 * 		Width of the frame
	 * @throws InterruptedException
	 */
	public Window(int Height, int Width) throws InterruptedException 
	{		
		this.setTitle("Fitness = f(number_of_generation)");
		this.setSize( Width,Height);
		this.setLocationRelativeTo(null);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setResizable(false);

		/*Initialization of the menu*/		
	    this.test1.add(item1);
	    this.test1.addSeparator();
	    this.test1.add(item2);
	    this.test2.add(item3);
	    this.test2.addSeparator();
	    this.test2.add(item4);
	    this.test2.addSeparator();
	    this.test2.add(item5);
	    this.test2.addSeparator();
	    this.test2.add(item6);
	    this.menuBar.add(test1);
	    this.menuBar.add(test2);
	    this.setJMenuBar(menuBar);
	    this.setVisible(true);

	    BoutonListener BL1 = new BoutonListener();
	    BoutonListener2 BL2 = new BoutonListener2();
	    BoutonListener3 BL3 = new BoutonListener3();
	    BoutonListener4 BL4 = new BoutonListener4();
	    BoutonListener5 BL5 = new BoutonListener5();
	    BoutonListener7 BL7 = new BoutonListener7();
	    item1.addActionListener(BL7);
	    item2.addActionListener(BL3);
		item3.addActionListener(BL4);
		item4.addActionListener(BL1);
		item5.addActionListener(BL2);
		item6.addActionListener(BL5);
	}	
	
	/**
	 * Modification of the panel
	 * 
	 * @param actualPointCalculator
	 * 			Object which contain all the points to print
	 * @param actualNumberOfGeneration
	 */
	void setPanel(PointCalculator actualPointCalculator, int actualNumberOfGeneration)
	{			
		pan = new Panneau(actualPointCalculator, actualNumberOfGeneration, mousePosX, mousePosY);
		this.setContentPane(pan);		
		this.setVisible(true);
		mousePosX=pan.getMousePosX();
		mousePosY=pan.getMousePosY();
	}
	/*Refresh the panel*/
	void refreshPanel ()
	{
		pan.repaint();
	}	
	
	/**
	 * Listener of the pause button 
	 */
	public class BoutonListener implements ActionListener
	{
		public void actionPerformed(ActionEvent e)
		{
			if (launch)
			{
				controleValue = 1;
				launch = false;
				pause = true;
			}			
		}
	}

	/**
	 * Listener of the start button 
	 */
	public class BoutonListener2 implements ActionListener 
	{
		public void actionPerformed(ActionEvent e)
		{
			if ((pause==true))
			{
				if(confWind.getConfOK()==false)
				{
		    	    JOptionPane.showMessageDialog(null, "You have to set the configuration", "Error", JOptionPane.ERROR_MESSAGE); 
				}
				else
				{
					controleValue = 2;
					launch=true;
				}
			}
			
		}
	}

	/**
	 * Listener of the stop button 
	 */
	public class BoutonListener3 implements ActionListener
	{
		public void actionPerformed(ActionEvent arg0) 
	    {
			if(launch == true)
			{
				controleValue = 1;
			}
			int option = JOptionPane.showConfirmDialog(null, "Do you want to stop the simulation ?", "Stop", JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE);		
			if(option == JOptionPane.OK_OPTION)
			{
				launch=false;
				pause=false;
				controleValue = 3;
		    }
	    }    
	}
	
	/**
	 * Listener of the open button 
	 */
	public class BoutonListener7 implements ActionListener 
	{
		public void actionPerformed(ActionEvent e)
		{
			int option = JOptionPane.showConfirmDialog(null, "Do you want to launch with a file ?", "", JOptionPane.YES_NO_CANCEL_OPTION,JOptionPane.QUESTION_MESSAGE);			 
			if(option == JOptionPane.OK_OPTION)
			{
				SelectFile select= new SelectFile();
				select.choose();
				if(select.getCancel()==false)
				{
					result=select.getFile();
					file=true;
				}
			}
		}
	}	
	
	/**
	 * Listener of the launch button 
	 */
	public class BoutonListener4 implements ActionListener
	{
		public void actionPerformed(ActionEvent arg0) 
		{
			if(confWind.getConfOK()==false)
			{
	    	    JOptionPane.showMessageDialog(null, "You have to set the configuration", "Error", JOptionPane.ERROR_MESSAGE); 
			}
			controleValue = 4;
			if(pause == false && confWind.getConfOK()==true)
			{
				launch=true;
			}
		}   
	}	

	/**
	 * Listener of the configuration button 
	 */
	public class BoutonListener5 implements ActionListener
	{
		public void actionPerformed(ActionEvent arg0) 
		{				
			if(launch==false && pause==false)
			{
				confWind = new ConfigurationWindow(null, "Configuration", true);
				controleValue=10;
			}
		}   
	}		
	/*Function set*/	
	public void setControleValue(int value)
	{
		controleValue = value;
	}
	public void setFile(File file)
    {
    	result=file;
    }
	public void setFile(boolean value)
	{
		file = value;
	}	
	public void setLauch(boolean value)
	{
		launch=value;
	}
	/*Function get*/

	public boolean getFile()
	{
		return file;
	}
	public File getFileInit()
    {
    	return result;
    }	
	public ConfigurationWindow getConfigurationWindow()
	{
		return confWind;
	}
	public int getControleValue()
	{
		return controleValue;
	}	
	
	public Panneau getPann()
	{
		return pan;
	}
}
