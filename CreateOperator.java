/*
 *     Copyright (C) <2013>  <DESCHAU Flavien VASSAL Adrien>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package pack;
import java.util.Random;


public class CreateOperator implements ICreateOperator
{
	/**
	 * Create a population of solution
	 * 
	 * @param conf
	 * 		A configuration file to know how to construct the population
	 * @return A table of chromosome which represent the created population
	 * 
	 * @see ICreateOperator
	 */
	public Chromosome [] creatPopulation(Configuration conf)
	{	
		int chromosomeLength = conf.getChromosomeLength();
		int numberOfChromosome = conf.getNumberOfChromosome();
		int offset = conf.getBorneInf();
		Random rnd = new Random();
		Chromosome[] myPopulation = new Chromosome[numberOfChromosome];
		
		switch (conf.getGeneType())
		{
		case 1 :
			/* Case : little population*/		
			if((int)(Math.pow(2,chromosomeLength)/numberOfChromosome)>=2)
			{
				int value1, offset1=0;
				for(int i=0;i<numberOfChromosome;i++)
				{
					myPopulation[i] = new Chromosome(conf);
					value1 = offset1 + rnd.nextInt((int)(Math.pow(2,chromosomeLength)/numberOfChromosome));
					offset1+=(int)(Math.pow(2,chromosomeLength)/numberOfChromosome);				
					myPopulation[i].setChromosomeList(stringToBoolean(Integer.toBinaryString(value1), chromosomeLength));	
				}
			}			
			/*Case : medium population*/
			else if (Math.pow(2,chromosomeLength) > numberOfChromosome)
			{			
				int save=0;
				for (int i=0;i<Math.pow(2,chromosomeLength); i++)
				{
					if(i%2 !=0 )
					{			
						myPopulation[save] = new Chromosome(conf);
						myPopulation[save].setChromosomeList(stringToBoolean(Integer.toBinaryString(i), chromosomeLength));	
						save++;
					}
				}
				for(int i=save;i<numberOfChromosome;i++)
				{
					myPopulation[i] = new Chromosome(conf);
					for(int j=0;j<chromosomeLength; j++)
					{	
						myPopulation[i].setGeneAt(j,(rnd.nextBoolean()));
					}	
				}
			}			
			/*Case : big population*/
			else
			{		
				for (int i=0;i<Math.pow(2,chromosomeLength); i++)
				{				
					myPopulation[i] = new Chromosome(conf);
					for(int j=0;j<chromosomeLength;j++)
					{
						myPopulation[i].setChromosomeList(stringToBoolean(Integer.toBinaryString(i), chromosomeLength));
					}	
				}
				for(int i= (int)(Math.pow(2,chromosomeLength));i<numberOfChromosome;i++)
				{
					myPopulation[i] = new Chromosome(conf);
					for(int j=0;j<chromosomeLength; j++)
					{	
						myPopulation[i].setGeneAt(j,(rnd.nextBoolean()));
					}				
				}
			}
			return myPopulation;			
		case 2 :
			/* Case : little population*/
			int offset2 = conf.getBorneInf();	
			int value2; 
			if((conf.getBorneSup()-conf.getBorneInf())/numberOfChromosome>=2)
			{
				for(int i = 0;i<numberOfChromosome;i++)
				{
					myPopulation[i] = new Chromosome(conf);
					value2 = offset+offset2+ rnd.nextInt((int)((conf.getBorneSup()-conf.getBorneInf())/numberOfChromosome));
					offset2+=(int)((conf.getBorneSup()-conf.getBorneInf())/numberOfChromosome);
					for(int j = 0;j<chromosomeLength;j++)
					{
						myPopulation[i].setGeneAt(j,value2);
					}
				}
			}			
			/*Case : medium population*/
			else if ((conf.getBorneSup()-conf.getBorneInf())>numberOfChromosome)
			{
				int save = 0;
				for(int i = 0;i<(conf.getBorneSup()-conf.getBorneInf());i++)
				{
					if(i%2 != 0)
					{
						myPopulation[save] = new Chromosome(conf);
						for(int j = 0;j<chromosomeLength;j++)
						{
							myPopulation[save].setGeneAt(j,i);
						}
						save++;
					}					
				}
				for(int i=save;i<numberOfChromosome;i++)
				{
					myPopulation[i] = new Chromosome(conf);
					for(int j=0;j<chromosomeLength; j++)
					{	
						myPopulation[i].setGeneAt(j,(rnd.nextInt(conf.getBorneSup()-conf.getBorneInf())));
					}	
				}
			}
			/* Case : big population*/
			else
			{		
				for (int i=0;i<(conf.getBorneSup()-conf.getBorneInf()); i++)
				{				
					myPopulation[i] = new Chromosome(conf);
					for(int j=0;j<chromosomeLength;j++)
					{						
						myPopulation[i].setGeneAt(j,i);
					}
				}
				for(int i= (conf.getBorneSup()-conf.getBorneInf());i<numberOfChromosome;i++)
				{
					myPopulation[i] = new Chromosome(conf);
					for(int j=0;j<chromosomeLength; j++)
					{							
						myPopulation[i].setGeneAt(j,(rnd.nextInt(conf.getBorneSup()-conf.getBorneInf())));
					}				
				}
			}			
			return myPopulation;
		case 3 :
			/* Case : little population*/					
			if((conf.getBorneSup()-conf.getBorneInf())/numberOfChromosome>=2)
			{
				double value3, offset3=0;
				for(int i = 0;i<numberOfChromosome;i++)
				{
					myPopulation[i] = new Chromosome(conf);
					value3 = (double)offset+offset3+ rnd.nextDouble()*(int)((conf.getBorneSup()-conf.getBorneInf())/numberOfChromosome);
					offset3+=(double)((conf.getBorneSup()-conf.getBorneInf())/numberOfChromosome);
					for(int j = 0;j<chromosomeLength;j++)
					{
						myPopulation[i].setGeneAt(j,value3);
					}
				}
			}			
			/*Case : medium population*/
			else if ((conf.getBorneSup()-conf.getBorneInf())>numberOfChromosome)
			{
				int save = 0;
				for(int i = 0;i<(conf.getBorneSup()-conf.getBorneInf());i++)
				{
					if(i%2 != 0)
					{
						myPopulation[save] = new Chromosome(conf);
						for(int j = 0;j<chromosomeLength;j++)
						{
							myPopulation[save].setGeneAt(j,(double)i+rnd.nextDouble());
						}
						save++;
					}					
				}
				for(int i=save;i<numberOfChromosome;i++)
				{
					myPopulation[i] = new Chromosome(conf);
					for(int j=0;j<chromosomeLength; j++)
					{	
						myPopulation[i].setGeneAt(j,(rnd.nextDouble()*(conf.getBorneSup()-conf.getBorneInf())));
					}	
				}
			}
			/* Case : big population*/
			else
			{		
				for (int i=0;i<(conf.getBorneSup()-conf.getBorneInf()); i++)
				{				
					myPopulation[i] = new Chromosome(conf);
					for(int j=0;j<chromosomeLength;j++)
					{						
						myPopulation[i].setGeneAt(j,(double)i+rnd.nextDouble());
					}
				}
				for(int i= (conf.getBorneSup()-conf.getBorneInf());i<numberOfChromosome;i++)
				{
					myPopulation[i] = new Chromosome(conf);
					for(int j=0;j<chromosomeLength; j++)
					{							
						myPopulation[i].setGeneAt(j,(rnd.nextDouble()*(conf.getBorneSup()-conf.getBorneInf())));
					}				
				}
			}
			return myPopulation;
		case 4 :
			/* Case : little population*/					
			if((conf.getBorneSup()-conf.getBorneInf())/numberOfChromosome>=2)
			{
				long value3, offset3=0;
				for(int i = 0;i<numberOfChromosome;i++)
				{
					myPopulation[i] = new Chromosome(conf);
					value3 = (long)offset+offset3+ rnd.nextInt((int)((conf.getBorneSup()-conf.getBorneInf())/numberOfChromosome));
					offset3+=(long)((conf.getBorneSup()-conf.getBorneInf())/numberOfChromosome);
					for(int j = 0;j<chromosomeLength;j++)
					{
						myPopulation[i].setGeneAt(j,value3);
					}
				}
			}			
			/*Case : medium population*/
			else if ((conf.getBorneSup()-conf.getBorneInf())>numberOfChromosome)
			{
				int save = 0;
				for(long i = 0;i<(conf.getBorneSup()-conf.getBorneInf());i++)
				{
					if(i%2 != 0)
					{
						myPopulation[save] = new Chromosome(conf);
						for(int j = 0;j<chromosomeLength;j++)
						{
							myPopulation[save].setGeneAt(j,i);
						}
						save++;
					}					
				}
				for(int i=save;i<numberOfChromosome;i++)
				{
					myPopulation[i] = new Chromosome(conf);
					for(int j=0;j<chromosomeLength; j++)
					{	
						myPopulation[i].setGeneAt(j,((long)rnd.nextInt(conf.getBorneSup()-conf.getBorneInf())));
					}	
				}
			}
			/* Case : big population*/
			else
			{		
				for (long i=0;i<(conf.getBorneSup()-conf.getBorneInf()); i++)
				{				
					myPopulation[(int)(i)] = new Chromosome(conf);
					for(int j=0;j<chromosomeLength;j++)
					{						
						myPopulation[(int)(i)].setGeneAt(j,i);
					}
				}
				for(int i= (conf.getBorneSup()-conf.getBorneInf());i<numberOfChromosome;i++)
				{
					myPopulation[i] = new Chromosome(conf);
					for(int j=0;j<chromosomeLength; j++)
					{							
						myPopulation[i].setGeneAt(j,(long)(rnd.nextInt(conf.getBorneSup()-conf.getBorneInf())));
					}				
				}
			}
			return myPopulation;
		case 5 :
			/* Case : little population*/					
			if((conf.getBorneSup()-conf.getBorneInf())/numberOfChromosome>=2)
			{
				float value3, offset3=0;
				for(int i = 0;i<numberOfChromosome;i++)
				{
					myPopulation[i] = new Chromosome(conf);
					value3 = (float)offset+offset3+ rnd.nextFloat()*(int)((conf.getBorneSup()-conf.getBorneInf())/numberOfChromosome);
					offset3+=(float)((conf.getBorneSup()-conf.getBorneInf())/numberOfChromosome);
					for(int j = 0;j<chromosomeLength;j++)
					{
						myPopulation[i].setGeneAt(j,value3);
					}
				}
			}			
			/*Case : medium population*/
			else if ((conf.getBorneSup()-conf.getBorneInf())>numberOfChromosome)
			{
				int save = 0;
				for(float i = 0;i<(conf.getBorneSup()-conf.getBorneInf());i++)
				{
					if(i%2 != 0)
					{
						myPopulation[save] = new Chromosome(conf);
						for(int j = 0;j<chromosomeLength;j++)
						{
							myPopulation[save].setGeneAt(j,(float)i+rnd.nextFloat());
						}
						save++;
					}					
				}
				for(int i=save;i<numberOfChromosome;i++)
				{
					myPopulation[i] = new Chromosome(conf);
					for(int j=0;j<chromosomeLength; j++)
					{	
						myPopulation[i].setGeneAt(j,(rnd.nextFloat()*(conf.getBorneSup()-conf.getBorneInf())));
					}	
				}
			}
			/* Case : big population*/
			else
			{		
				for (float i=0;i<(conf.getBorneSup()-conf.getBorneInf()); i++)
				{				
					myPopulation[(int)(i)] = new Chromosome(conf);
					for(int j=0;j<chromosomeLength;j++)
					{						
						myPopulation[(int)(i)].setGeneAt(j,(float)i+rnd.nextFloat());
					}
				}
				for(int i = (conf.getBorneSup()-conf.getBorneInf());i<numberOfChromosome;i++)
				{
					myPopulation[i] = new Chromosome(conf);
					for(int j=0;j<chromosomeLength; j++)
					{							
						myPopulation[i].setGeneAt(j,(rnd.nextFloat()*(conf.getBorneSup()-conf.getBorneInf())));
					}				
				}
			}
			
			return myPopulation;
		
		default :
			System.out.println("ERROR in configuration Gene type");
			return myPopulation;
		}
	}
		
		/**
		 * Convert a string in to a table of boolean
		 * <p>
		 * This function is only used in the case of boolean gene
		 * <p>
		 * 
		 * @param value
		 * 		A string which represent a chromosome
		 * @param chromosomeLength
		 * 		The length of the current chromosome
		 * @return a table of boolean which represent a chromosome
		 */
		boolean[] stringToBoolean(String value, int chromosomeLength)
		{
			boolean[] myChromosome = new boolean[chromosomeLength];
			int save=0;
			if((chromosomeLength-value.length())>0)
			{
				for(int i=0;i<(chromosomeLength-value.length());i++)
				{
					myChromosome[i]=false;
					save++;
				}
				for(int i=0;i<value.length();i++)
				{
					if(value.charAt(i)=='0')
					{
						myChromosome[i+save]=false;
					}
					if(value.charAt(i)=='1')
					{
						myChromosome[i+save]=true;
					}
				}
			}
			else
			{
				for(int i=0;i<chromosomeLength;i++)
					
				{
					if(value.charAt(i)=='0')
					{
						myChromosome[i]=false;
					}
					if(value.charAt(i)=='1')
					{
						myChromosome[i]=true;
					}
				}
			}		
		return myChromosome;
	}
}