/*
 *     Copyright (C) <2013>  <DESCHAU Flavien VASSAL Adrien>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package pack;

import java.io.File;
import java.io.IOException;
 
import javax.swing.JFileChooser;
 
 
public class SelectFile
{
	/**
	 * The file that the user choose
	 */
	private File result;
	
	/**
	 * Validation of the use of the file
	 */
	private boolean cancel = false;
	
    public SelectFile()
    {
    }    
    
    /**
     * Open a file chooser for the user
     */
    public void choose()
    {
    	int statuEvent = 0;
        File repertoireCourant = null;
        cancel = false;
        
        try {
            repertoireCourant = new File(".").getCanonicalFile();
        } catch(IOException e) {}
        JFileChooser dialogue = new JFileChooser(repertoireCourant);
        statuEvent = dialogue.showOpenDialog(null);   
        result=dialogue.getSelectedFile();
        if(JFileChooser.CANCEL_OPTION==statuEvent)
        {
            System.out.println("je suis dedans");
        	cancel=true;
        }
    }
    
    /*Function set*/    
    public void setFile(File file)
    {
    	result=file;
    }
    public void setCancel(boolean value)
    {
    	cancel = value;
    }
    
    
    /*Function get*/
    public File getFile()
    {
    	return result;
    }    
    public boolean getCancel()
    {
    	return cancel;
    }
}