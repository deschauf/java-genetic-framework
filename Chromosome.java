/*
 *     Copyright (C) <2013>  <DESCHAU Flavien VASSAL Adrien>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package pack;
import java.util.ArrayList;
import java.util.List;

public class Chromosome extends Gene
{
	/**
	 * The list of Gene which represent a chromosome
	 * 
	 * @see DefaultGene
	 */
	private List<Gene> myChromosome;
	
	/**
	 * The fitness value of the current chromosome
	 */	
	private int fitnessValue;
	
	/**
	 * The Configuration object 
	 * <p> 
	 * This object is used to construct the chromosome
	 * <p>
	 * 
	 * @see Configuration
	 */
	private Configuration config;
	
	/**
	 * The number of Gene in a chromosome
	 */
	int chromosomeLength;

	/**
	 * Default constructor of Chromosome
	 */
	public Chromosome()
	{		
	}
	
	/**
	 * Constructor of Chromosome
	 * <p>
	 * The construction of the chromosome depend on the Configuration object
	 * <p>
	 * 
	 * @param conf
	 * 			The configuration object
	 * 
	 * @see Configuration
	 * @see Gene
	 */
			
	public Chromosome(Configuration conf)
	{
		super();
		setConfig(conf);
		fitnessValue=0;
		chromosomeLength = conf.getChromosomeLength();		
		myChromosome = new ArrayList<Gene> ();
		if(conf.getChromosomeConfig().length==0)
		{
			for(int i = 0;i<conf.getChromosomeLength();i++)
			{
				myChromosome.add(new Gene(0));	
			}
		}
		else
		{
			for(int i = 0;i<conf.getChromosomeConfig().length;i++)
			{
				myChromosome.add(new Gene(conf.getChromosomeConfig()[i]));
			}
		}
	}
	
	/**
	 * Get the list of Gene of the current chromosome
	 * 
	 * @return the list of Gene	 
	 */	
	public List<Gene> getChromosomeList()
	{
		return myChromosome;
	}
	
	/**
	 * Get the fitness value of the current chromosome
	 * 

	 * @return the fitness value 
	 */
	public int getChromosomeFitness()
	{
		return fitnessValue;
	}
	
	/**
	 * Get the gene at the given position
	 * 
	 * @param value 
	 * 			The position of the gene in the list of gene 'myChromosome'
	 * @return the gene at the given position
	 * 
	 * @see Gene
	 */
	public Object getGeneAt(int value)
	{
		return myChromosome.get(value).getIGene().getAllel(config);
	}
	
	/**
	 * get the configuration object
	 * 
	 * @return the configuration object of the current chromosome
	 * 
	 * @see Configuration
	 */	
	public Configuration getConfig() 
	{
		return config;
	}

	/**
	 * Set the fitness of the current chromosome
	 * 
	 * @param fitness
	 * 			The fitness of this chromosome
	 */
	public void setChromosomeFitness(int fitness)
	{
		fitnessValue = fitness;
	}
	
	/**
	 * Set the allele of Gene at the given position
	 * @param index
	 * 			The position of the Gene in the list of Gene 'myChromosome'
	 * @param value
	 * 			The value of the allele
	 * @see Gene
	 */	
	public void setGeneAt(int index, Object value)
	{
		myChromosome.get(index).getIGene().setAllel(value,config);
	}
	
	/**
	 * Set the list of Gene
	 * 
	 * @param myChromosomeList
	 * 			The list of Gene 
	 * 
	 * @see Gene
	 */	
	public void setChromosomeList(List<Gene> myChromosomeList)
	{
		myChromosome = myChromosomeList;
	}
	
	/**
	 * Set the list of Gene in case of boolean Gene
	 * 
	 * @param myChromosomeTable
	 * 			A table of boolean which represent the allele of the current chromosome
	 * 
	 * @see Gene
	 */
	public void setChromosomeList(boolean [] myChromosomeTable)
	{
		for(int i=0;i<chromosomeLength;i++)
		{			
			myChromosome.get(i).getIGene().setAllel(myChromosomeTable[i],config);
		}
	}
	
	/**
	 * Set the Configuration object
	 * 
	 * @param config
	 * 			The configuration object
	 * 
	 * @see Configuration
	 */
	public void setConfig(Configuration config) 
	{
		this.config = config;
	}
}