/*
 *     Copyright (C) <2013>  <DESCHAU Flavien VASSAL Adrien>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package pack;

public class Gene 
{		
	IGene gene;
	public Gene()
	{		
	}
	
	/**
	 * Create a new gene depending on the configuration object
	 * @param value
	 * 		Integer which represent the number of custom gene to use
	 */
	public Gene(int value)
	{
		switch (value)
		{
			case 0 :	
				gene = new DefaultGene();
			break;
			case 1 :
				gene = new UserGene0();
			break;
			case 2 :
				gene = new UserGene1();
			break;	
			case 3 :	
				gene = new UserGene2();
			break;
			case 4 :
				gene = new UserGene3();
			break;
			case 5 :
				gene = new UserGene4();
			break;
			case 6 :		
				gene = new UserGene5();
			break;
			case 7 :
				gene = new UserGene6();
			break;
			case 8 :
				gene = new UserGene7();
			break;
			case 9 :		
				gene = new UserGene8();
			break;
			case 10 :
				gene = new UserGene9();
			break;
		}		
	}
	
	public IGene getIGene()
	{
		return gene;
	}	
}