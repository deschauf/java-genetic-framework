/*
 *     Copyright (C) <2013>  <DESCHAU Flavien VASSAL Adrien>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package pack;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.swing.JFrame;

public class PointCalculator
{
	/**
	 * List of point to print which represent the average of the fitness of the last generations
	 */
	private List<Point> pointToPrintAvg = new ArrayList<Point>();
	
	/**
	 * List of point to print which represent the maximum of the fitness of the last generations
	 */
	private List<Point> pointToPrintMax = new ArrayList<Point>();
	
	/**
	 * List of the last maximum fitness
	 */
	private List<Integer> ListFitnessMax = new ArrayList<Integer>();
	
	/**
	 * List of the last average of fitness
	 */
	private List<Integer> ListFitnessAverage = new ArrayList<Integer>();
	
	/**
	 * Actual fitness scale
	 */
	private int actualFitnessScale = 10;
	
	/**
	 * Actual generation scale
	 */
	private int actualGenerationScale = 10;	
	
	/**
	 * Actual number of generation
	 */
	private int numberOfGeneration = 0;
	
	/**
	 * Actual height
	 */
	private int height = 0;
	
	/**
	 * actual width
	 */
	private int width = 0;
	
	/**
	 * actual graphic height
	 */
	private int graphicHeight = 0;
	
	/**
	 * actual graphic width
	 */
	private int graphicWidth = 0;
	
	/**
	 * Best chromosome of the last population
	 */
	private Chromosome bestChromosome;
	
	/**
	 * A configuration object
	 */
	private Configuration config;
	
	JFrame myFenetre = new JFrame();
	/**
	 * Initialize the point calculator
	 * @param actualHeight
	 * @param actualWidth
 	*/
	PointCalculator(int actualHeight, int actualWidth) 
	{
		height = actualHeight;
		width = actualWidth;
		graphicSizeCalculator();
	}

	/**
	 * Calculate the position of all the different points to print
	 * @param ActualPopulation
	 * 
	 * @see population
	 */
	
	void setPointCalculator(Population ActualPopulation)
	{
		Point pointMax = new Point();
		Point pointAvg = new Point();
		int averageOfListFitnessAverage=0;
		int averageOfListFitnessMax=0;		
		ListFitnessMax.add(ActualPopulation.bestSolution().getChromosomeFitness());
		ListFitnessAverage.add(ActualPopulation.fitnessAverage());
		numberOfGeneration = ListFitnessMax.size();
		
		bestChromosome=ActualPopulation.bestSolution();
		
		/*Calculate a new point each actualGenerationScale/10*/
		
		if(numberOfGeneration%(actualGenerationScale/10)==0)
		{
			averageOfListFitnessMax=averagePoint(ListFitnessMax);
			averageOfListFitnessAverage=averagePoint(ListFitnessAverage);
			
			if(ListFitnessMax.size()>= actualGenerationScale*10 && actualGenerationScale*10>ActualPopulation.getNumberOfGeneration())
			{
				actualGenerationScale*=10;
			}				
			else if(ListFitnessMax.size()>= actualGenerationScale*10)
			{
				actualGenerationScale = ActualPopulation.getNumberOfGeneration()/10;
			}
			setFitnessScale();

			pointAvg.setScaleGene(actualGenerationScale);
			pointAvg.setScaleFit(actualFitnessScale);
			pointAvg.setFit(averageOfListFitnessAverage);
			pointAvg.setGen(numberOfGeneration);
			pointAvg.position=placePoint(numberOfGeneration,averageOfListFitnessAverage);
			pointToPrintAvg.add(pointAvg);
			
			pointMax.setScaleGene(actualGenerationScale);
			pointMax.setScaleFit(actualFitnessScale);
			pointMax.setFit(averageOfListFitnessMax);
			pointMax.setGen(numberOfGeneration);
			pointMax.position=placePoint(numberOfGeneration,averageOfListFitnessMax);
			pointToPrintMax.add(pointMax);

			for(int i=0;i<pointToPrintAvg.size();i++)
			{	
				if(pointToPrintAvg.get(i).scaleFit!=actualFitnessScale || pointToPrintAvg.get(i).scaleGeneration!=actualGenerationScale)
				{
					pointToPrintAvg.get(i).setPosition(placePoint(numberOfGeneration,pointToPrintAvg.get(i).fit)[1],1);
					pointToPrintAvg.get(i).setPosition((pointToPrintAvg.get(i).position[0]-40)*pointToPrintAvg.get(i).getScaleGene()/actualGenerationScale+40,0);
					pointToPrintAvg.get(i).setScaleGene(actualGenerationScale);
					pointToPrintAvg.get(i).setScaleFit(actualFitnessScale);
				}
				
				if(pointToPrintMax.get(i).scaleFit!=actualFitnessScale || pointToPrintMax.get(i).scaleGeneration!=actualGenerationScale)
				{
					pointToPrintMax.get(i).setPosition(placePoint(numberOfGeneration,pointToPrintMax.get(i).fit)[1],1);
					pointToPrintMax.get(i).setPosition((pointToPrintMax.get(i).position[0]-40)*pointToPrintMax.get(i).getScaleGene()/actualGenerationScale+40,0);
					pointToPrintMax.get(i).setScaleGene(actualGenerationScale);
					pointToPrintMax.get(i).setScaleFit(actualFitnessScale);
				}
			}
		}
	}
	
	
	
	
	/**
	 * return the position of a point depending on his fitness
	 * @param generation
	 * 		The actual number of generation
	 * @param fit
	 * 		The fitness of the current chromosome
	 * @return a table of two integer which represent the X ant Y position of the point
	 */
	
	int[] placePoint( int generation, int fit)
	{		
		int position[]=new int[2]; 

		/*Setting the offset of the window*/
		position[1]=height-93;
		position[0]=40;

		/*Set the number of unites (the unites depend on the scale)*/
		if(fit/actualFitnessScale != 0)
		{
			position[1]-=fit/actualFitnessScale*graphicHeight/10;
			fit=fit%actualFitnessScale;
		}			
		/*Set the number smaller than the unite*/
		position[1]-=fit*graphicHeight/(actualFitnessScale*10);
		position[0]+=generation*graphicWidth/(actualGenerationScale*10);
		
		return position;
	}
	
	/**
	 * Calculate the average of the fitness for the last X generation 
	 * @param actualValueFitness
	 * 			A list of all the last fitness value
	 * @return An integer which represent the average of the fitness for the last X generation
	 */
	int averagePoint(List<Integer> actualValueFitness)
	{
		int average = 0;
		if(actualValueFitness.size()==1)
		{
			if(actualValueFitness.get(0)<0)
				return 0;
			else
				return actualValueFitness.get(0);
		}
		else
		{			
			for(int i = actualValueFitness.size()-actualGenerationScale/10;i<actualValueFitness.size();i++)
			{
				average +=actualValueFitness.get(i);				
			}
			average/=(actualGenerationScale/10);
			if(average<0)
				return 0;
			else
			return average;
		}
	}	
	

	/**
	 * Calculate the size allocate to the graphic
	 */
	
	void graphicSizeCalculator()
	{
		graphicHeight = height-108;
		graphicWidth = width -286;
		while(graphicHeight%10!=0)
		{
			graphicHeight-=1;
		}
		while(graphicWidth%10!=0)
		{
			graphicWidth-=1;
		}
	}
	
	/**
	 * Set the fitness scale
	 */
	void setFitnessScale ()
	{
		int newFitnessScale;
		if(Collections.max(ListFitnessMax)>=Collections.max(ListFitnessAverage))
		{
			newFitnessScale=Collections.max(ListFitnessMax);
		}
		else
		{
			newFitnessScale=Collections.max(ListFitnessAverage);
		}
		if(newFitnessScale>10*actualFitnessScale)
		{
			while(newFitnessScale%10!=0)
			{
				newFitnessScale--;
			}
			actualFitnessScale = newFitnessScale/10 + newFitnessScale/25;
		}
	}
	
	/**
	 * Return a string of the best solution
	 * 
	 * @return	A string which represent the best solution
	 */
	
	String ChrToString()
	{
		char toStr[] = new char[config.getChromosomeLength()];
		String str = new String();
		switch (config.getGeneType())
		{
		case 1 :
			for(int i=0;i<config.getChromosomeLength();i++)
			{
				str = ""+bestChromosome.getGeneAt(i);
				if (str.equals("true"))
				{
					toStr[i]='1';
				}
				else if (str.equals("false"))
				{
					toStr[i]='0';
				}
			}
		break;
			
		case 2 :
			for(int i=0;i<config.getChromosomeLength();i++)
			{
				str +=bestChromosome.getGeneAt(i)+" ";
			}
		return str;
			
		case 3 :
			for(int i=0;i<config.getChromosomeLength();i++)
			{
				str += ((double) ((int) ((double)bestChromosome.getGeneAt(i)*100))) / 100 +" ";				
			}
		return str;
			
		case 4 :
			for(int i=0;i<config.getChromosomeLength();i++)
			{
				str += bestChromosome.getGeneAt(i)+" ";				
			}
		return str;
			
		case 5 :
			for(int i=0;i<config.getChromosomeLength();i++)
			{
				str +=((float) ((int) ((float)bestChromosome.getGeneAt(i)*100))) / 100 +" ";
			}
		return str;
		}
		String str2 = new String(toStr);
		return str2;
	}
	
	/*
	 * setter
	 */
	
	public void setBestChromosome(Chromosome chr)
	{
		bestChromosome=chr;
	}
	
	public void setActualFitnessScale(int value)
	{
		actualFitnessScale=value;
	}
	
	public void setActualGenerationScale(int value)
	{
		actualGenerationScale=value;
	}
	public void setValueFintnessAvg(List<Integer> value)
	{
		ListFitnessAverage=value;
	}
	
	public void setValueFitnessMax(List<Integer> value)
	{
		ListFitnessAverage=value;
	}
	public void setConfig(Configuration conf)
	{
		config=conf;
	}
	
	/*
	 * getter
	 */
	
	public List<Point> getPointToPrintMax()
	{
		return pointToPrintMax;
	}
	
	public List<Point> getPointToPrintAvg()
	{
		return pointToPrintAvg;
	}
	
	public int getPointToPrintMaxAt(int i, int axe)
	{

		return pointToPrintMax.get(i).position[axe];
	}
	
	public int getPointToPrintAvgAt(int i, int axe)
	{
		return pointToPrintAvg.get(i).position[axe];
	}
	
	public int getActualFitnessScale()
	{
		return actualFitnessScale;
	}
	
	public int getActualGenerationScale()
	{
		return actualGenerationScale;
	}
	
	public int getNumberOfGeneration()
	{
		return numberOfGeneration;
	}
	
	public int getGraphicalHeight()
	{
		return graphicHeight;
	}
	
	public int getGraphicalWidth()
	{
		return graphicWidth;
	}
	public Chromosome getBestChromosome()
	{
		return bestChromosome;
	}
	
	public Configuration getConfig()
	{
		return config;
	}
	
}
