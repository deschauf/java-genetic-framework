/*
 *     Copyright (C) <2013>  <DESCHAU Flavien VASSAL Adrien>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package pack;

import java.io.File;
import java.io.IOException;

import javax.swing.JFrame;

public class GraphicalMain 
{
	/**
	 * Object which calculate the position of the point on the graphic
	 */
	private PointCalculator myPointCalculator;	
	
	/**
	 * The window
	 */
	private Window myWindow;
	
	/**
	 * boolean to indicate if the user want to quit or not
	 */
	private int quit = 0;
	
	/**
	 * Boolean used to know is the simulation is paused or not
	 */
	private boolean pause = false;
	
	/**
	 * The population of solutions
	 */
	private Population pop;
	
	/**
	 * A Configuration object
	 * 
	 * @see Configuration
	 */
	private Configuration conf;
	
	/**
	 * A file to print the result of the simulation in
	 */
	private File result = new File("result.txt");
	
	/**
	 * Sub-main which managed all the graphical and genetic part
	 * 
	 * @param width
	 * 		width of the window
	 * @param height
	 * 		height of the window
	 * @param config
	 * @throws InterruptedException
	 * @throws IOException
	 * 
	 * @see Configuration
	 */
	
	public GraphicalMain(int width, int height, Configuration config) throws InterruptedException, IOException
	{	
		myWindow = new Window(height, width);		
		quit = JFrame.EXIT_ON_CLOSE;
		conf = new Configuration();	
		myPointCalculator = new PointCalculator(height, width);
		int i = 0;
		while(quit == 3)
		{	
			switch (myWindow.getControleValue())
			{
				/*Pause the simulation*/
				case 1 :
					myWindow.setControleValue(0);
					pause = true;
					/*Continue to actualize the Graphic for the cursor position*/
					if(i!=0)
					{
						while(myWindow.getControleValue() != 2 && myWindow.getControleValue() != 3)
						{
							RefreshGraphical();
							try {
								Thread.sleep(50);
							} catch (InterruptedException e) {
								e.printStackTrace();
							}
						}
					}
				break;
				
				/*Start the simulation*/
				case 2 :	
					if(pause == true)
					{
						
						pause = false;
						myWindow.setControleValue(0);
						while(i<myWindow.getConfigurationWindow().getConfigurationWindowInfo().getnumberOfGeneration() && myWindow.getControleValue()!=3 && myWindow.getControleValue() != 1)
						{
							PrintGraphical(pop);
							pop.printInFile(pop, result, i);
							i++;
						}			
						if(i>=myWindow.getConfigurationWindow().getConfigurationWindowInfo().getnumberOfGeneration())
						{
							i=0;
							/*Continue to actualize the Graphic for the cursor position*/
							while(myWindow.getControleValue() != 3 && myWindow.getControleValue() != 4)
							{
								RefreshGraphical();
								try {
									Thread.sleep(50);
								} catch (InterruptedException e){
									e.printStackTrace();
								}
							}
							myPointCalculator = new PointCalculator(height, width);
						}
					}
				break;
				
				/*Stop the simulation*/
				case 3 :
					pause = false;
					myWindow.setControleValue(0);
					conf = new Configuration();	
					myPointCalculator = new PointCalculator(height, width);
					if(i!=0)
					{
						myWindow.getPann().setVisible(false);
						i = 0;
					}
				break;

				/*Launched the simulation*/
				case 4 :
					Thread.sleep(50);
					myWindow.setControleValue(0);
					if(myWindow.getConfigurationWindow().getConfOK() == true && pause == false)
					{
						/*Set the configuration*/
						i=0;
						conf.setChromosomeLength(myWindow.getConfigurationWindow().getConfigurationWindowInfo().getChromosomeLength());
						conf.setNumberOfChromosome(myWindow.getConfigurationWindow().getConfigurationWindowInfo().getnumberOfChromosome());
						conf.setNumberOfGeneration(myWindow.getConfigurationWindow().getConfigurationWindowInfo().getnumberOfGeneration());
						conf.setGeneType(config.getGeneType());
						conf.setInterval(config.getBorneInf(),config.getBorneSup());
						if(myWindow.getConfigurationWindow().getConfigurationWindowInfo().getBoolCreation()==true)
						{
							conf.setCreateOperator(config.getCreateOperator());
						}
						else
						{
							conf.setCreateOperator(new CreateOperator());
						}
						if(myWindow.getConfigurationWindow().getConfigurationWindowInfo().getBoolCrossover()==true)
						{
							conf.setCrossoverOperator(config.getCrossoverOperator());
						}
						else
						{
							conf.setCrossoverOperator(new CrossoverOperator());
						}
						if(myWindow.getConfigurationWindow().getConfigurationWindowInfo().getBoolMutation()==true)
						{
							conf.setMutationOperator(config.getMutationOperator());
						}
						else
						{
							conf.setMutationOperator(new MutateOperator());
						}
						if(myWindow.getConfigurationWindow().getConfigurationWindowInfo().getBoolGene()==true)
						{
							conf.setChromosomeConfig(config.getChromosomeConfig());
						}
						else
						{
							conf.setChromosomeConfig();
						}
						if(myWindow.getConfigurationWindow().getConfigurationWindowInfo().getNumberSelectionOperator()==0)
						{
							conf.setSelectionOperator(new SelectionTournamentProbability());
						}
						else if (myWindow.getConfigurationWindow().getConfigurationWindowInfo().getNumberSelectionOperator()==1)
						{
							conf.setSelectionOperator(new SelectionTournament());
						}
						else if (myWindow.getConfigurationWindow().getConfigurationWindowInfo().getNumberSelectionOperator()==2)
						{
							conf.setSelectionOperator(new SelectionBiasedWheel());
						}
						else if (myWindow.getConfigurationWindow().getConfigurationWindowInfo().getNumberSelectionOperator()==3)
						{
							conf.setSelectionOperator(config.getSelectionOperator());
						}
						myPointCalculator.setConfig(conf);
						pop = new Population(conf);
						/*Set from a file or not*/
						if(myWindow.getFile()==true)
						{
							pop.initialisePopFromFile(myWindow.getFileInit(), myWindow.getConfigurationWindow().getConfigurationWindowInfo().getnumberOfChromosome());
						}
						else
						{
							pop.createRndPopulation();
						}
						myWindow.setFile(false);					
						/*beginning of the generation*/
						while(i<myWindow.getConfigurationWindow().getConfigurationWindowInfo().getnumberOfGeneration() && myWindow.getControleValue()!= 1 && myWindow.getControleValue() != 3)
						{							
							pop.evolve();
							PrintGraphical(pop);
							pop.printInFile(pop, result, i);
							i++;
						}
						myWindow.setLauch(false);
						if(i>=myWindow.getConfigurationWindow().getConfigurationWindowInfo().getnumberOfGeneration())
						{
							/*Continue to actualize the Graphic for the cursor position*/
							while(myWindow.getControleValue() != 1 && myWindow.getControleValue() != 3 && myWindow.getControleValue() != 4)
							{								
								RefreshGraphical();
								try {
									Thread.sleep(50);
								} catch (InterruptedException e) {
									e.printStackTrace();
								}
							}
							myPointCalculator = new PointCalculator(height, width);
						}						
					}	
				break;
				default :
				break;
			}	
		}
	}
	
	/**
	 * Print the new graphical
	 * 
	 * @param myPopulation
	 * 		The population to print on the graphic
	 * 
	 * @see PointCalculator
	 */
	
	void PrintGraphical(Population myPopulation)
	{
			myPointCalculator.setPointCalculator(myPopulation);
			myWindow.setPanel(myPointCalculator, myPopulation.getNumberOfGeneration());
	}	
	
	public void RefreshGraphical()
	{
		myWindow.refreshPanel();		
	}
	
	/*
	 * Function get
	 */
	
	public boolean getPause()
	{
		return pause;
	}
	
	int getQuit()
	{
		return quit;
	}

	/*
	 * Function set
	 */
	
	public void setPause(boolean value)
	{
		pause=value;
	}

}

