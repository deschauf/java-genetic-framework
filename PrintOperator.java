/*
 *     Copyright (C) <2013>  <DESCHAU Flavien VASSAL Adrien>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package pack;

public class PrintOperator implements IPrintOperator 
{
	/**
	 * Print the current population in the console
	 * 
	 * @param conf
	 * @param myPopulation
	 * 		Population to print in the console
	 * 
	 * @see IPrintOperator
	 */
	public void printPop(Configuration conf, Chromosome myPopulation[])
	{
		if(conf.getGeneType()==0)
		{
			for (int i=0;i<myPopulation.length;i++)
			{	
				System.out.println("Chromosome " + i +" fitness " + myPopulation[i].getChromosomeFitness());
				for (int j=0;j<conf.getChromosomeLength();j++)
				{
					if ((Boolean)myPopulation[i].getGeneAt(j)==true)
					{
						System.out.print(1);
					}
					else if ((Boolean)myPopulation[i].getGeneAt(j)==false)
					{
						System.out.print(0);
					}
				}
				System.out.print("\n");
			}
		}
		else 
		{
			for (int i=0;i<myPopulation.length;i++)
			{	
				System.out.println("Chromosome " + i +" fitness " + myPopulation[i].getChromosomeFitness());
				for (int j=0;j<conf.getChromosomeLength();j++)
				{
					System.out.println(myPopulation[i].getGeneAt(j));
				}
				System.out.print("\n");
			}
		}
	}
}
