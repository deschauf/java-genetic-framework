/*
 *     Copyright (C) <2013>  <DESCHAU Flavien VASSAL Adrien>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package pack;

import java.util.Random;


public class MutateOperator implements IMutateOperator
{

	/**
	 * Create a mutate population with a given population
	 * 
	 * @param conf 
	 * @param myPopulation
	 * 
	 * @see Chromosome
	 * @see Configuration
	 */
	public Chromosome[] mutation(Configuration conf, Chromosome myPopulation[])
	{
		Random rdn = new Random();
		int random;
		int numberOfMutation=0;
		Chromosome mutatePopulation[];
		
		/*
		 * get number of mutation ( probability of mutation : 1/100 )
		 */
		
		for(int i=0;i<conf.getNumberOfChromosome();i++)
		{
			if(rdn.nextInt(100)==50)
			{	
				numberOfMutation++;
			}
		}		
		
		mutatePopulation = new Chromosome[numberOfMutation];
		
		/*
		 * Operator of mutation 
		 */

		for(int i=0; i<numberOfMutation;i++)
		{
			mutatePopulation[i]= new Chromosome(conf);
			random = rdn.nextInt(conf.getChromosomeLength());
			switch (conf.getGeneType())
			{
			case 1 :
				for(int j=0;j<conf.getChromosomeLength();j++)
				{
					mutatePopulation[i].setGeneAt(random,!(Boolean)mutatePopulation[i].getGeneAt(random));
				}	
			break;
			case 2 :
				int random2 = rdn.nextInt(conf.getBorneSup()-conf.getBorneInf());
				for(int j=0;j<conf.getChromosomeLength();j++)
				{
					mutatePopulation[i].setGeneAt(random, random2);
				}	
			break;
			case 3 :
				double random3 = rdn.nextDouble()*(conf.getBorneSup()-conf.getBorneInf());
				for(int j=0;j<conf.getChromosomeLength();j++)
				{
					mutatePopulation[i].setGeneAt(random, random3);
				}
			break;
			case 4 :
				long random4 = rdn.nextInt((conf.getBorneSup()-conf.getBorneInf()));
				for(int j=0;j<conf.getChromosomeLength();j++)
				{
					mutatePopulation[i].setGeneAt(random, random4);
				}
			break;
			case 5 :
				float random5 = rdn.nextFloat()*(conf.getBorneSup()-conf.getBorneInf());
				for(int j=0;j<conf.getChromosomeLength();j++)
				{
					mutatePopulation[i].setGeneAt(random, random5);
				}
			break;
			}
		}		
		return mutatePopulation;
	}
}
