/*
 *     Copyright (C) <2013>  <DESCHAU Flavien VASSAL Adrien>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package pack;

import java.io.File;
import java.io.IOException;

public class InternMain 
{
	/**
	 * A Configuration object
	 * 
	 * @see Configuration
	 */
	private Configuration conf;
	
	/**
	 * A file to print the result in
	 */
	private File result;
	
	/**
	 * The population of solutions
	 */
	private Population pop; 
	
	/**
	 * Sub-main which managed all the genetic part
	 * <p>
	 * This sub-main is used when the user dosen't want a graphic
	 * <p>
	 * 
	 * @param config
	 * @param file
	 * 
	 * @see Configuration
	 */
	public InternMain(Configuration config, File file)
	{
		conf = config;
		result = file;
		pop =  new Population(conf);		
	}
	
	/**
	 * Overloading of the sub-main which managed all the genetic part
	 * <p>
	 * This sub-main is used when the user dosen't want a graphic
	 * <p>	 
	 * 
	 * @param config
	 * 
	 * @see Configuration
	 */
	public InternMain(Configuration config)
	{
		conf = config;
		pop =  new Population(config);		
	}
	
	/**
	 * Start an evolution
	 * 
	 * @throws IOException
	 */
	public void start() throws IOException
	{ 
		pop.createRndPopulation();
		for(int i=0; i<conf.getNumberOfGeneration();i++)
		{
				pop.evolve();
				if(conf.getPrintInFile())
				{
					pop.printInFile(pop, result, i);
				}
		}
		print(pop.bestSolution());
	}
	
	/**
	 * Start an evolution from a file
	 * 
	 * @param file
	 * @throws IOException
	 */
	public void start(File file) throws IOException
	{
		pop.initialisePopFromFile(file, conf.getNumberOfChromosome());
		for(int i=0; i<conf.getNumberOfGeneration();i++)
		{
				pop.evolve();
				if(conf.getPrintInFile())
				{
					pop.printInFile(pop, result, i);
				}

		}
		print(pop.bestSolution());
	}
	
	
	/**
	 * Print the best solution
	 * @param chr
	 * 		The best solution
	 * 
	 * @see Chromosome
	 */
	void print(Chromosome chr)
	{
		System.out.println("best solution: ");
		for (int i=0;i<conf.getChromosomeLength();i++)
		{
			System.out.print(chr.getGeneAt(i)+" ");
		}
		System.out.println("\tfitness: "+ chr.getChromosomeFitness());
	}
	
	/* get */
	public Population getPop()
	{
		return pop;
	}
	
	/*  set */
	public void setPop(Population value)
	{
		pop = value;
	}
		
}
