package pack;

import java.util.Random;
/*Just a test to verify the functionality of the configuration object*/
public class TestCrossover implements ICrossoverOperator
{
	public Chromosome [] crossover(Configuration conf, Chromosome myPopulation[])
	{
	int numberOfChromosome = conf.getNumberOfChromosome();
	int chromosomeLength = conf.getChromosomeLength();
	int index[] = new int[numberOfChromosome];
	int crossPoint, random, test=numberOfChromosome;
	Chromosome chromosomeSon1;
	Chromosome chromosomeSon2;
	Chromosome crossoverPopulation[];
	Chromosome actualChromosome = new Chromosome(conf);
	Random rnd = new Random();
	int numberOfCrossover=0;
		
	/*
	 * Determine the number of mutation
	 */
	
	for(int i=0; i<numberOfChromosome;i++)
	{
		if(rnd.nextInt(9)>=3)
		{
			numberOfCrossover++;
		}
	}

	/*
	 * Maximize the number of mutation
	 */
	
	if(numberOfChromosome%2 == 0)
	{
		if(numberOfCrossover > (numberOfChromosome/2))
		{
			numberOfCrossover = numberOfChromosome/2;
		}
	}
	if(numberOfChromosome%2 != 0)
	{
		if(numberOfCrossover > ((numberOfChromosome-1)/2))
		{
			numberOfCrossover = (numberOfChromosome-1)/2;
		}
	}
	
	/*
	 * Initialize the table of Chromosome
	 */
	
	crossoverPopulation = new Chromosome[2*numberOfCrossover];
	
	for(int i=0; i<(2*numberOfCrossover);i++)
	{
		crossoverPopulation[i] = new Chromosome(conf);
	}	

	/*
	 * Initialize the table of index
	 */
	
	for(int i=0; i<numberOfChromosome;i++)
	{
		index[i]=1;
	}		

	for(int i=0; i<numberOfChromosome;i++)
	{		
		if(index[i]!=0)	
		{	
			test--;
			index[i]=0;			
			if(test >=1 && numberOfCrossover >0)	
			{				
				test--;
				do
				{
				random = rnd.nextInt(numberOfChromosome-i);	
				actualChromosome = myPopulation[i+random];
				}
				while (index[i+random]==0);	
					
				index[random+i]=0;
				crossPoint = rnd.nextInt(chromosomeLength);		
				chromosomeSon1 = new Chromosome(conf);
				chromosomeSon2 = new Chromosome(conf);
				for(int j=0;j<chromosomeLength;j++)	
				{						
					if(j<=crossPoint)
					{
						chromosomeSon1.setGeneAt(j,actualChromosome.getGeneAt(j));
						chromosomeSon2.setGeneAt(j,myPopulation[i].getGeneAt(j));
					}
					if(j>crossPoint)
					{
						chromosomeSon1.setGeneAt(j,myPopulation[i].getGeneAt(j));
						chromosomeSon2.setGeneAt(j,actualChromosome.getGeneAt(j));
					}				
				}
				crossoverPopulation[i].setChromosomeList(chromosomeSon1.getChromosomeList());
				crossoverPopulation[crossoverPopulation.length-i-1].setChromosomeList(chromosomeSon2.getChromosomeList());

				numberOfCrossover--;
			}
		}
	}	
	return crossoverPopulation;
	}
}
